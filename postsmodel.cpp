#include "postsmodel.h"
#include <QJsonArray>
#include <QJsonValue>
#include <QDateTime>

PostsModel::PostsModel(QObject *parent) : QAbstractListModel(parent)
{
  _roles[Post::TitleRole] = "title";
  _roles[Post::UrlRole] = "url";
  _roles[Post::ContentRole] = "text";
  _roles[Post::ReadRole] = "read";
  _roles[Post::PubDateRole] = "pub_date";
}

void PostsModel::setPosts(const QList<Post*> &posts)
{
  _posts = posts;
}

int PostsModel::rowCount(const QModelIndex &) const
{
  return _posts.size();
}

QVariant PostsModel::data(const QModelIndex &index, int role) const
{
    int r = index.row();
    if ((r<0) || (r>=_posts.count()))
        return QVariant::Invalid;

    auto post =  _posts.at(r);
    switch (role) {
    break; case Qt::DisplayRole: // The default display role now displays the first name as well
      return QVariant::fromValue(post->title());
    break; case Post::TitleRole:
      return QVariant::fromValue(post->title());
    break; case Post::UrlRole:
      return QVariant::fromValue(post->url());
    break; case Post::ContentRole:
      return QVariant::fromValue(post->content());
    break; case Post::ReadRole:
      return QVariant::fromValue(post->isRead());
    break; case Post::PubDateRole:
      return QVariant::fromValue(QDateTime::fromTime_t(post->pubDate()).toString("yy-MM-dd hh:mm"));
    break; default:
      return QVariant();
    }
}

QList<QString> PostsModel::roles()
{
  QList<QString> ans;
  foreach(QByteArray b, _roles.values())
    ans << QString(b);
  return ans;
}

Post *PostsModel::get(int idx) const
{
  return _posts.at(idx);
}

QHash<int, QByteArray> PostsModel::roleNames() const
{
  return _roles;
}


Post::Post(const Post &that, QObject *parent): QObject(parent),
                                               _title(that._title), _url(that._url), _content(that._content),
                                               _read(that._read), _pub_date(that._pub_date), _feed(that._feed) {}

Post::Post(QObject *parent) : QObject(parent) {}

QString Post::title() const
{
  return _title;
}

void Post::setTitle(const QString & title)
{
  _title = title;
}

QUrl Post::url() const
{
  return _url;
}

void Post::setUrl(const QUrl &url)
{
  _url = url;
}

QString Post::content() const
{
  return _content;
}

void Post::setContent(const QString& text)
{
  _content = text;
}

bool Post::isRead() const
{
  return _read;
}

void Post::setRead(bool read)
{
  _read = read;
}

uint Post::pubDate() const
{
  return _pub_date;
}

void Post::setPubDate(uint pub_date)
{
  _pub_date = pub_date;
}

QUrl Post::feed() const
{
  return _feed;
}

void Post::setFeed(const QUrl &feed)
{
  _feed = feed;
}

QJsonObject Post::toJson() const
{
  QJsonObject post_obj;
  post_obj.insert("title",QJsonValue(title()));
  post_obj.insert("url",QJsonValue(url().toString()));
  post_obj.insert("read", QJsonValue(isRead()));
  post_obj.insert("content",QJsonValue(content()));
  post_obj.insert("pub_date",QJsonValue((int)pubDate()));
  post_obj.insert("feed", QJsonValue(url().toString()));
  return post_obj;
}

bool Post::fromJson(const QJsonObject & post_obj)
{
  try {
    auto title = post_obj.value("title");
    if(!title.isString())
      return false;
    auto url = post_obj.value("url");
    if(!url.isString())
      return false;
    auto read = post_obj.value("read");
    if(!read.isBool())
      return false;
    auto content = post_obj.value("content");
    if(!content.isString())
      return false;
    auto pub_date = post_obj.value("pub_date");
    if(!pub_date.isDouble())
      return false;
    auto feed_url = post_obj.value("feed");
    if(!feed_url.isString())
      return false;
    setTitle(title.toString());
    setUrl(QUrl(url.toString()));
    setRead(read.toBool());
    setContent(content.toString());
    setPubDate((uint) pub_date.toDouble());
    setFeed(feed_url.toString());
  } catch(...) {
    return false;
  }
  return true;
}



