#ifndef FEEDPOLLER_H
#define FEEDPOLLER_H

#include <QObject>
#include <QThread>
#include <QUrl>
#include <QNetworkReply>

class MainModel;
class Post;
class QNetworkAccessManager;
class QDomElement;

class FeedPoller : public QObject {
  Q_OBJECT
public:
  explicit FeedPoller(QObject *parent = 0);
  ~FeedPoller();
signals:
  void sigFinishedLoadingFeed(QString, QString, QString);
  void sigFinishedLoadingPost(Post*);
  void sigParse(QUrl, QString);
  void sigError(QUrl, QString);
  void sigProgress(QUrl, qint64,qint64);
public slots:
  void slotError(QNetworkReply::NetworkError);
  void slotSslError(const QList<QSslError>&);
  void slotProgress(qint64,qint64);
  void slotReplyFinished(QNetworkReply *reply);
public:
  void poll(const QUrl& feed_url);
  void poll(const QList<QUrl>& feed_urls);
private:
  QThread _work_thread;
  QNetworkAccessManager *_manager;
};

class FeedParser: public QObject {
  Q_OBJECT
public slots:
  bool slotParse(const QUrl& feed_url, const QString& content);
signals:
  void sigFinishedLoadingFeed(QString,QString,QString);
  void sigFinishedLoadingPost(Post*);
private:
  static QString OptionalField(const QDomElement& elem, const QString& name);
  static QDateTime PubDateToDateTime(const QString& pub_date_str);
};

#endif // FEEDPOLLER_H
