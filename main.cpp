#include "mainmodel.h"
#include "feedsmodel.h"
#include "postsmodel.h"
#include "modelstorer.h"
#include <QtGui/QGuiApplication>
#include <QtQml>
#include <QQmlContext>
#include <QUrl>
#include <QStandardPaths>
#include "qtquick2applicationviewer.h"

int main(int argc, char *argv[])
{
  qmlRegisterType<Post>("Post", 1, 0, "Post");
  qmlRegisterType<Feed>("Feed", 1, 0, "Feed");
  qmlRegisterType<PostsModel>("PostsModel", 1, 0, "PostsModel");
  qmlRegisterType<FeedsModel>("FeedsModel", 1, 0, "FeedsModel");
  qmlRegisterType<MainModel>("MainModel", 1, 0, "MainModel");
  qRegisterMetaType<FeedsModel*>("FeedsModel");
  qRegisterMetaType<PostsModel*>("PostsModel");
  qRegisterMetaType<Post*>("Post");
  QGuiApplication app(argc, argv);

  QtQuick2ApplicationViewer viewer;
  QString store_file = QStandardPaths::locate(QStandardPaths::DataLocation, "store.json");
  MainModel* model = ModelStorer::FromFile(store_file, &viewer);

  viewer.setTitle(QObject::tr("Twinkle Reader"));
  viewer.setHeight(400);
  viewer.setWidth(300);
  viewer.showExpanded();
  viewer.rootContext()->setContextProperty("main_model", model);
  viewer.setMainQmlFile(QStringLiteral("qml/tr/Main.qml"));

  QIcon icon(":/icons/twinklereader80.png");
  viewer.setIcon(icon);
  return app.exec();
}
