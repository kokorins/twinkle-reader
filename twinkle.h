#ifndef TWINKLE_H
#define TWINKLE_H
#include <QObject>
#include <QStringList>
#include <QMap>
#include <QPixmap>
#include <QUrl>
class QJsonObject;
class QTimer;

struct TwinkleOptions {
  int rate;
  QString splitters;
  QJsonObject toJson()const;
  bool fromJson(const QJsonObject&);
public:
  TwinkleOptions();
};

struct TwinkleState {
  int idx;
  QStringList words;
  QMap<QString, QUrl> links;
  QMap<QString, QPixmap> imgs;
};

class Twinkle: public QObject {
  Q_OBJECT
public:
  explicit Twinkle(const TwinkleOptions& to, QObject *parent = 0);
  void populateContent(const QString& content);
  ~Twinkle();
  void setOptions(const TwinkleOptions& to);
  const TwinkleOptions& options()const;
signals:
  void sigCurrentWord(QString);
public slots:
  void start();
  void pause();
  void stop();
private slots:
  void next();
private:
  TwinkleState Parse(const QString& content);
private:
  TwinkleOptions _to;
  QTimer* timer;
  TwinkleState _state;
};

#endif // TWINKLE_H
