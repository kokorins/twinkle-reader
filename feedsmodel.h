#ifndef FEEDSMODEL_H
#define FEEDSMODEL_H

#include <QAbstractTableModel>
#include <QUrl>
#include <QList>
#include <QObject>
#include <QHash>
#include <QJsonObject>

class Post;

class Feed : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString title READ title WRITE setTitle)
  Q_PROPERTY(QUrl url READ url WRITE setUrl)
  Q_PROPERTY(int num_unread READ numUnread WRITE setNumUnread NOTIFY sigNumUnread)
  Q_PROPERTY(QList<QUrl> posts READ posts WRITE setPosts)
public:
  enum FeedRoles {
      TitleRole = Qt::UserRole + 1,
      UrlRole = Qt::UserRole + 2,
      NumUnreadRole = Qt::UserRole + 3,
      PostsRole = Qt::UserRole + 4
    };
public:
  explicit Feed(QObject *parent = 0);
  QString title() const;
  void setTitle(const QString &);
  QUrl url()const;
  void setUrl(const QUrl& url);
  int numUnread()const;
  void setNumUnread(int num_unread);
  const QList<QUrl>& posts()const;
  void setPosts(const QList<QUrl>& posts);
  void addPost(Post* post);
  QJsonObject toJson()const;
  bool fromJson(const QJsonObject&);
signals:
  void sigNumUnread();
private:
  QString _title;
  QUrl _url;
  int _num_unread;
  QList<QUrl> _posts;
};

class FeedsModel : public QAbstractListModel {
  Q_OBJECT
  Q_PROPERTY(int count READ rowCount CONSTANT)
public:
public:
  explicit FeedsModel(QObject *parent = 0);
  explicit FeedsModel(const QList<Feed *> &feeds, QObject *parent = 0);
  void setFeeds(const QList<Feed *> &feeds);
  Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
  QVariant data(const QModelIndex &index, int role) const;
  Q_INVOKABLE QList<QString> roles();
  Q_INVOKABLE Feed* get(int idx) const;
  virtual QHash<int, QByteArray> roleNames() const;
private:
  Q_DISABLE_COPY(FeedsModel)
private:
  QList<Feed*> _feeds;
  QHash<int,QByteArray> _roles;
};

#endif // FEEDSMODEL_H
