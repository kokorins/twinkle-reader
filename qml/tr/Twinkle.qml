import QtQuick 2.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0

Rectangle {
    id: twinkle
    width:300;
    height: 400

    RowLayout {
        id: title_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.top: parent.top
        anchors.topMargin: 3

        ToolButton {
            id: back_button
            text: qsTr("Back")
            iconSource: "qrc:icons/arrow-180"
            onClicked: {
                main_model.slotPauseTwinkle()
                loader.source = "Posts.qml"
            }
        }

        Text {
            id: title_label
            text: main_model.post.title
            wrapMode: Text.WordWrap
        }
        ColumnLayout{}
    }

    RowLayout {
        id: player_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.bottom: act_layout.top
        anchors.bottomMargin: 3
        ColumnLayout {}
        ToolButton {
            id: play_button
            text: qsTr("Play")
            iconSource: "qrc:icons/control"
            onClicked: main_model.slotStartTwinkle()
        }
        ToolButton {
            id: pause_button
            text: qsTr("Pause")
            iconSource: "qrc:icons/pause-pause"
            onClicked: main_model.slotPauseTwinkle()
        }
        ToolButton {
            id: stop_button
            text: qsTr("Stop")
            iconSource: "qrc:icons/control-stop-square"
            onClicked: main_model.slotStopTwinkle()
        }
    }

    RowLayout {
        id: act_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3
        Text {
            id: status
            text: main_model.status
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
        }
        ColumnLayout {}

        ToolButton {
            id: switch_button
            text: qsTr("Switch")
            iconSource: "qrc:icons/switch"
            onClicked: {
                main_model.slotPauseTwinkle()
                previous = "Twinkle.qml"
                main_model.post_view = 0
                loader.source = "Post.qml"
            }
        }
        ToolButton {
            id: open_source_button
            text: qsTr("Open source")
            iconSource: "qrc:icons/feed--arrow"
            onClicked: {
                main_model.slotPauseTwinkle()
                main_model.slotOpenSource()
            }
        }

        ToolButton {
            id: options_button
            text: qsTr("Options")
            iconSource: "qrc:icons/property"
            onClicked: {
                main_model.slotPauseTwinkle()
                previous = "Twinkle.qml"
                loader.source = "Options.qml"
            }
        }

    }

    Text {
        id: word_text
        anchors.centerIn: parent
        text: main_model.word
        font.pointSize: 24
    }
}
