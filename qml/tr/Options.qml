import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0

Rectangle {
    id: options
    width:300
    height: 400

    ColumnLayout {
        id: main_grid
        anchors.bottom: act_layout.top
        anchors.bottomMargin: 3
        anchors.top: title_layout.bottom
        anchors.topMargin: 3
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        RowLayout{
            Text {
                id: rate_lable
                text: qsTr("Twinkle Rate")
            }
            SpinBox {
                id: twinkle_rate
                value: main_model.twinkle_rate
                suffix: qsTr(" ms")
                minimumValue: 50
                maximumValue: 1000
            }
        }
        RowLayout{
            Text {
                id: interval_lable
                text: qsTr("Update interval")
            }
            SpinBox {
                id: update_interval
                value: main_model.update_interval
                suffix: qsTr(" s")
                minimumValue: 0
                maximumValue: 3600
            }
        }

        RowLayout {}
    }

    RowLayout {
        id: title_layout
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 3
        anchors.topMargin: 3
        anchors.rightMargin: 3
        ToolButton {
            id: back_button
            text: qsTr("Back")
            iconSource: "qrc:icons/arrow-180"
            onClicked: {
                loader.source = previous
            }
        }
        Text {
            id: title_text
            text: qsTr("Options")
        }
        RowLayout {}
    }

    RowLayout {
        id: act_layout
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 3
        anchors.bottomMargin: 3
        anchors.rightMargin: 3

        Text {
            id: status
            text: main_model.status
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
        }
        ColumnLayout {}
        ToolButton {
            id: export_session_button
            iconSource: "qrc:icons/drive-download"
            onClicked:export_dialog.open()
        }

        ToolButton {
            id: import_session_button
            iconSource: "qrc:icons/drive-upload"
            onClicked: import_dialog.open()
        }

        ToolButton {
            id: apply_button
            text: qsTr("Apply")
            iconSource: "qrc:icons/tick"
            onClicked: {
                main_model.twinkle_rate = twinkle_rate.value
                main_model.update_interval = update_interval.value
                loader.source = previous
            }
        }
    }
    FileDialog {
        id: export_dialog
        title: qsTr("Please choose a file to save")
        selectExisting: false
        onAccepted: {
            main_model.slotSaveSession(export_dialog.fileUrl)
        }
    }
    FileDialog {
        id: import_dialog
        title: qsTr("Please choose a file to save")
//        selectedNameFilter: qsTr("Json files (*.json)")
        onAccepted: {
            main_model.slotLoadSession(import_dialog.fileUrl)
        }
    }
}

