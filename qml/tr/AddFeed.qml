import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0

Rectangle {
    id: add_feed
    width: 300
    height: 400

    ColumnLayout {
        id: main_layout
        anchors.top: title_layout.bottom
        anchors.bottom: act_layout.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 3
        anchors.rightMargin: 3
        RowLayout {
            id: name_row
            height: 32
            anchors.right: parent.right
            anchors.rightMargin: 3
            anchors.left: parent.left
            anchors.leftMargin: 3
            TextField {
                id: title_text
                anchors.fill: parent
                placeholderText: qsTr("Feed Title")
            }
        }
        RowLayout {
            id: url_row
            height: 32
            anchors.left: parent.left
            anchors.leftMargin: 3
            anchors.right: parent.right
            anchors.rightMargin: 3
            TextField {
                id: url_text
                placeholderText: "Feed Url"
                focus: true
//                validator: RegExpValidator{regExp: /(https|http|ftp)\:\/\/|([a-z0-9A-Z]+\.[a-z0-9A-Z]+\.[a-zA-Z]{2,4})|([a-z0-9A-Z]+\.[a-zA-Z]{2,4})|\?([a-zA-Z0-9]+[\&\=\#a-z]+)/i}
                anchors.fill: parent
            }
        }
        RowLayout {}
    }

    RowLayout {
        id: title_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.top: parent.top
        anchors.topMargin: 3
        ToolButton {
            id: back_button
            text: qsTr("Back")
            iconSource: "qrc:icons/arrow-180"
            onClicked: {
                loader.source = "Feeds.qml"
            }
        }
        Text {
            id: title_lable
            text: qsTr("Add Feed")
        }
        ColumnLayout{}
    }

    RowLayout {
        id: act_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3

        Text {
            id: status
            text: main_model.status
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
        }
        ColumnLayout {}
        ToolButton {
            id: add_button
            text: qsTr("Another")
            iconSource: "qrc:icons/plus"
            onClicked: {
                main_model.slotAddFeed(title_text.text, url_text.text,"")
                title_text = ""
                url_text = ""
            }
        }

        ToolButton {
            id: add_update_button
            text: qsTr("Add")
            iconSource: "qrc:icons/tick"
            onClicked: {
                main_model.slotAddFeed(title_text.text, url_text.text, "")
                loader.source = "Feeds.qml"
            }
        }
    }
}
