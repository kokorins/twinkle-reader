import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import Feed 1.0
import FeedsModel 1.0
import MainModel 1.0

Rectangle {
    id: feeds_rect
    width:300
    height: 400
    Connections {
        target: main_model
        onSigUpdateViews: feeds.model = main_model.feeds_model
    }

/* //Model
    ListModel {
        id : feeds_model
        ListElement {
                name: "Woboq"
                unread: 0
                url : "woboq.com"
            }
            ListElement {
                name: "Habra"
                unread: 1
                url : "habrahabr.ru"
            }
    }
*/
    Component {
        id : feeds_delegate
        Item {
            width: parent.width
            height: txt.height
            Text {
                id : txt
                anchors.leftMargin: 3
                anchors.rightMargin: 3
                anchors.left: parent.left
                anchors.right: parent.right
                verticalAlignment: Text.AlignVCenter
                text: "%1 (%2)".arg(model.title).arg(model.num_unread)
                wrapMode: Text.WordWrap
            }
            MouseArea {
                id : mouse_click
                anchors.fill: parent
                onClicked: {
                    feeds.currentIndex = index
                }
                onDoubleClicked: {
                    feeds.currentIndex = index
                    loader.source = "Posts.qml"
                }
            }
        }
    }

    ListView {
        id: feeds
        anchors.top: title_layout.bottom
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.bottom: act_layout.top
        anchors.bottomMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.topMargin: 3
        spacing: 3
        model:main_model.feeds_model
        delegate: feeds_delegate
        highlight: Rectangle {color: "lightsteelblue"; radius: 3; opacity: 0.7 }
        focus: true
        onCurrentIndexChanged: if(currentIndex>-1 && main_model.feeds_model.rowCount()>0)
                                   main_model.slotFeedSelected(main_model.feeds_model.get(currentIndex).url)
    }

    RowLayout {
        id: title_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.top: parent.top
        anchors.topMargin: 3
        Text {
            id: title_text
            text: qsTr("Feeds")
        }
    }


    RowLayout {
        id: act_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3

        Text {
            id: status
            text: main_model.status
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
        }
        ColumnLayout {}
        ToolButton {
            id : options_button
            text : qsTr("Options")
            iconSource: "qrc:icons/property"
            onClicked: {
                previous = "Feeds.qml"
                loader.source = "Options.qml"
            }
        }
        ToolButton {
            id : remove_feed_button
            text : qsTr("Remove Feed")
            iconSource: "qrc:icons/minus"
            onClicked: {
                if(feeds.currentIndex>-1 && main_model.feeds_model.rowCount()>0) {
                    main_model.slotRemoveFeed(main_model.feeds_model.get(feeds.currentIndex).url)
                    feeds.currentIndex = -1
                }
            }
        }
        ToolButton {
            id : add_feed_button
            text : qsTr("Add Feed")
            iconSource: "qrc:icons/plus"
            onClicked: {
                previous = "Feeds.qml"
                loader.source = "AddFeed.qml"
            }
        }
        ToolButton {
            id: update_button
            text: qsTr("Update")
            iconSource: "qrc:icons/arrow-circle-double"
            onClicked: main_model.slotUpdateFeeds()
        }
    }
}
