import QtQuick 2.0
import QtWebKit 3.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import Post 1.0
import MainModel 1.0

Rectangle {
    id: post_window
    width:300
    height: 400

    WebView {
        id : web
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: title_layout.bottom
        anchors.bottom: act_layout.top
        anchors.topMargin: 3
        anchors.bottomMargin: 3
        url : main_model.post.url
        onUrlChanged: {console.log("Url changed");
            web.loadHtml(main_model.post.content,
                         main_model.post.url,
                         main_model.post.url)}
    }

    RowLayout {
        id: title_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.top: parent.top
        anchors.topMargin: 3

        ToolButton {
            id: back_button
            text: qsTr("Back")
            iconSource: "qrc:icons/arrow-180"
            onClicked: {
                loader.source = "Posts.qml"
            }
        }

        Text {
            id: title_label
            text: main_model.post.title
        }
        ColumnLayout{}
    }

    RowLayout {
        id: act_layout
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3

        Text {
            id: status
            text: main_model.status
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
        }
        ColumnLayout {}

        ToolButton {
            id: switch_button
            text: qsTr("Switch")
            iconSource: "qrc:icons/switch"
            onClicked: {
                previous = "Post.qml"
                main_model.post_view = 1
                loader.source = "Twinkle.qml"
            }
        }

        ToolButton {
            id: options_button
            text: qsTr("Options")
            iconSource: "qrc:icons/property"
            onClicked: {
                previous = "Post.qml"
                loader.source = "Options.qml"
            }
        }
    }
}
