import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.0
import MainModel 1.0
import PostsModel 1.0
import Post 1.0

Rectangle {
    id: posts_rect
    width:300
    height: 400

    Connections {
        target: main_model
        onSigUpdateViews: posts.model = main_model.posts_model
    }

    Component {
        id : posts_delegate
        Item {
            anchors.left: parent.left
            anchors.right: parent.right
            height: txt.height
            Column {
                id:txt
                anchors.left: parent.left
                anchors.right: parent.right
                Text {
                    id : title
                    anchors.leftMargin: 3
                    anchors.rightMargin: 3
                    anchors.left: parent.left
                    anchors.right: parent.right
                    text: model.title
                    color: if(model.read) {"black"} else {"red"}
                    wrapMode: Text.WordWrap
                }
                Text {
                    id: pub_date
                    anchors.leftMargin: 3
                    anchors.rightMargin: 3
                    anchors.left: parent.left
                    anchors.right: parent.right
                    horizontalAlignment: Text.AlignRight
                    text: model.pub_date
                    color: "gray"
                }
            }
            MouseArea {
                id : mouse_click
                anchors.fill: parent
                onClicked: {
                    posts.currentIndex = index
                }
                onDoubleClicked: {
                    posts.currentIndex = index
                    if(main_model.post_view === 0)
                        loader.source = "Post.qml"
                    else if(main_model.post_view === 1)
                        loader.source = "Twinkle.qml"
                    else {
                        console.debug("Unknown output qml page")
                        loader.source = "Post.qml"
                    }
                }
            }
        }
    }

    ListView {
        id: posts
        anchors.top: title_layout.bottom
        anchors.topMargin: 3
        anchors.bottom: act_layout.top
        anchors.bottomMargin: 3
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        spacing: 3
        delegate: posts_delegate
        model: main_model.posts_model
        highlight: Rectangle { color: "lightsteelblue"; radius: 5; opacity: 0.7 }
        focus: true
        currentIndex: -1
        onCurrentIndexChanged: main_model.slotPostSelected(main_model.posts_model.get(currentIndex).url)
    }

    RowLayout {
        id: title_layout
//        height: 36
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.top: parent.top
        anchors.topMargin: 3

        ToolButton {
            id: back_button
            text: qsTr("Back")
            iconSource: "qrc:icons/arrow-180"
            onClicked: {
                loader.source = "Feeds.qml"
            }
        }
        Text {
            id: title_text
            text: qsTr("Posts")
        }
        ColumnLayout{}
    }

    RowLayout {
        id: act_layout
//        height: 36
        anchors.right: parent.right
        anchors.rightMargin: 3
        anchors.left: parent.left
        anchors.leftMargin: 3
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 3
        Text {
            id: status
            text: main_model.status
            wrapMode: Text.WordWrap
            elide: Text.ElideRight
        }
        ColumnLayout {}
        Button {
            id: mark_all_button
            text: qsTr("All Read")
            onClicked: main_model.slotMarkReadFeed(main_model.cur_feed_url)
        }
        ToolButton {
            id: update_button
            iconSource: "qrc:icons/arrow-circle-double"
            text: qsTr("Update")
            onClicked: main_model.slotUpdateCurrentFeed()
        }
    }
}
