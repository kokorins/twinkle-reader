#include "feedsmodel.h"
#include "postsmodel.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonValue>

FeedsModel::FeedsModel(QObject *parent) : QAbstractListModel(parent)
{
  _roles[Feed::TitleRole] = "title";
  _roles[Feed::UrlRole] = "url";
  _roles[Feed::NumUnreadRole] = "num_unread";
}

FeedsModel::FeedsModel(const QList<Feed*> &feeds, QObject *parent): QAbstractListModel(parent), _feeds(feeds)
{
  _roles[Feed::TitleRole] = "title";
  _roles[Feed::UrlRole] = "url";
  _roles[Feed::NumUnreadRole] = "num_unread";
}

void FeedsModel::setFeeds(const QList<Feed*> &feeds)
{
  _feeds = feeds;
}

int FeedsModel::rowCount(const QModelIndex &) const
{
  return _feeds.size();
}

QVariant FeedsModel::data(const QModelIndex &index, int role) const
{
    int r = index.row();
    if ((r<0) || (r>=_feeds.count()))
        return QVariant::Invalid;

    auto feed =  _feeds.at(r);
    switch (role) {
    break; case Qt::DisplayRole: // The default display role now displays the first name as well
      return QVariant::fromValue(feed->title());
    break; case Feed::TitleRole:
      return QVariant::fromValue(feed->title());
    break; case Feed::UrlRole:
      return QVariant::fromValue(feed->url());
    break; case Feed::NumUnreadRole:
      return QVariant::fromValue(feed->numUnread());
    break; default:
      return QVariant();
    }
}

QList<QString> FeedsModel::roles()
{
  QList<QString> ans;
  foreach(QByteArray b, _roles.values())
    ans << QString(b);
  return ans;
}

Feed *FeedsModel::get(int idx) const
{
  if(idx>=_feeds.size())
    return 0;
  return _feeds.at(idx);
}

QHash<int, QByteArray> FeedsModel::roleNames() const
{
  return _roles;
}

Feed::Feed(QObject *parent) : QObject(parent), _num_unread(0) {}

QString Feed::title() const
{
  return _title;
}

void Feed::setTitle(const QString & title)
{
  _title = title;
}

QUrl Feed::url() const
{
  return _url;
}

void Feed::setUrl(const QUrl &url)
{
  _url = url;
}

int Feed::numUnread() const
{
  return _num_unread;
}

void Feed::setNumUnread(int num_unread)
{
  _num_unread = num_unread;
  emit sigNumUnread();
}

const QList<QUrl> &Feed::posts()const
{
  return _posts;
}

void Feed::setPosts(const QList<QUrl> &posts)
{
  _posts = posts;
}

void Feed::addPost(Post *post)
{
  if(_posts.contains(post->url()))
    return;
  _posts.append(post->url());
  if(!post->isRead())
    ++_num_unread;
}

QJsonObject Feed::toJson() const
{
  QJsonObject feed_obj;
  feed_obj.insert("title",QJsonValue(title()));
  feed_obj.insert("url",QJsonValue(url().toString()));
  feed_obj.insert("num_unread", QJsonValue(numUnread()));
  QJsonArray post_urls;
  foreach(const QUrl&post_url, posts())
    post_urls.append(post_url.toString());
  feed_obj.insert("post_urls", post_urls);
  return feed_obj;
}

bool Feed::fromJson(const QJsonObject & feed_obj)
{
  try {
    auto title = feed_obj.value("title");
    if(!title.isString())
      return false;
    auto url = feed_obj.value("url");
    if(!url.isString())
      return false;
    auto num_unread = feed_obj.value("num_unread");
    if(!num_unread.isDouble())
      return false;
    auto feed_urls_obj = feed_obj.value("post_urls");
    if(!feed_urls_obj.isArray())
      return false;
    QJsonArray post_urls_array = feed_urls_obj.toArray();
    QList<QUrl> post_urls;
    for(auto it = post_urls_array.begin(); it!=post_urls_array.end(); ++it)
      post_urls<<QUrl((*it).toString());
    setTitle(title.toString());
    setUrl(QUrl(url.toString()));
    setNumUnread((int)num_unread.toDouble());
    setPosts(post_urls);
  } catch(...) {
    return false;
  }
  return true;
}
