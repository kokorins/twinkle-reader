#ifndef MODELSTORER_H
#define MODELSTORER_H

#include <QObject>
#include <QJsonDocument>

class MainModel;

class ModelStorer {
public:
  static void ToFile(QString file, const MainModel&);
  static QJsonDocument ToJson(const MainModel&);
  static MainModel* FromJson(const QJsonDocument& doc, QObject*parent = 0);
  static void FromJson(const QJsonDocument& doc, MainModel&);
  static MainModel* FromFile(const QString& file, QObject*parent=0);
  static void FromFile(QString file, MainModel&);
};

#endif // MODELSTORER_H
