#ifndef POSTSMODEL_H
#define POSTSMODEL_H

#include <QObject>
#include <QAbstractTableModel>
#include <QUrl>
#include <QJsonObject>

class Post : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY sigTitleChanged)
  Q_PROPERTY(QUrl url READ url WRITE setUrl NOTIFY sigUrlChanged)
  Q_PROPERTY(QString content READ content WRITE setContent NOTIFY sigTextChanged)
  Q_PROPERTY(bool read READ isRead WRITE setRead NOTIFY sigReadChanged)
public:
  enum PostRoles {
      TitleRole = Qt::UserRole + 1,
      UrlRole = Qt::UserRole + 2,
      ContentRole = Qt::UserRole + 3,
      ReadRole = Qt::UserRole + 4,
      PubDateRole = Qt::UserRole + 5
    };
public:
  Post(const Post& that, QObject *parent = 0);
  Post(QObject *parent = 0);
  QString title() const;
  void setTitle(const QString &);
  QUrl url()const;
  void setUrl(const QUrl& url);
  QString content()const;
  void setContent(const QString& content);
  bool isRead()const;
  void setRead(bool read);
  uint pubDate() const;
  void setPubDate(uint pub_date);
  QUrl feed() const;
  void setFeed(const QUrl &feed);
  QJsonObject toJson()const;
  bool fromJson(const QJsonObject&);
signals:
  void sigTitleChanged(const QString&);
  void sigUrlChanged(const QUrl&);
  void sigTextChanged(const QString&);
  void sigReadChanged(bool);
private:
  QString _title;
  QUrl _url;
  QString _content;
  bool _read;
  uint _pub_date;
  QUrl _feed;
};


class PostsModel : public QAbstractListModel
{
  Q_OBJECT
  Q_PROPERTY(int count READ rowCount CONSTANT)
public:
  explicit PostsModel(QObject *parent = 0);
  void setPosts(const QList<Post *> &posts);
  Q_INVOKABLE int rowCount(const QModelIndex &parent = QModelIndex()) const;
  QVariant data(const QModelIndex &index, int role) const;
  Q_INVOKABLE QList<QString> roles();
  Q_INVOKABLE Post* get(int idx)const;
  virtual QHash<int, QByteArray> roleNames() const;
private:
    Q_DISABLE_COPY(PostsModel)
private:
  QList<Post*> _posts;
  QHash<int,QByteArray> _roles;
};

#endif // POSTSMODEL_H
