#include "mainmodel.h"
#include "feedsmodel.h"
#include "postsmodel.h"
#include "feedsmodel.h"
#include "postsmodel.h"
#include "feedpoller.h"
#include "twinkle.h"
#include "modelstorer.h"
#include "qtquick2applicationviewer.h"

#include <QDir>
#include <QStandardPaths>
#include <QDebug>
#include <QTimer>
#include <QtWidgets/QSystemTrayIcon>
#include <QtQuick/QQuickWindow>
#include <QtQuick/qtquickglobal.h>
#include <QApplication>
#include <QDesktopServices>

MainModel::MainModel(QObject *parent) : QObject(parent)
{
  _twinkle = new Twinkle(TwinkleOptions(), this);
  connect(_twinkle, SIGNAL(sigCurrentWord(QString)), this, SLOT(slotWordChanged(QString)));
  _check_timer = new QTimer(this);
  connect(_check_timer, SIGNAL(timeout()), this, SLOT(slotUpdateFeeds()));
  if(_state.interval>0) {
    _check_timer->setInterval(1000*_state.interval);
    _check_timer->start();
  }
  QIcon icon(":/icons/twinklereader80.png");
  if(QSystemTrayIcon::isSystemTrayAvailable()) {
    _tray = new QSystemTrayIcon(icon, this);
    _tray->show();
//    _tray->showMessage(tr("Hello"), tr("I've started"));
    connect(_tray, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(slotTrayActivated()));
    connect(_tray, SIGNAL(messageClicked()), this, SLOT(slotTrayActivated()));
  }
  else {
    _tray = 0;
  }
}

MainModel::~MainModel()
{
  QString data_path = QStandardPaths::writableLocation(QStandardPaths::DataLocation);
  QDir dir(data_path);
  QString store_file = dir.filePath("store.json");
  qDebug()<<"Store to:"<<store_file;
  foreach(Feed* f, _feeds.values())
    updateNumUnread(f->url());
  ModelStorer::ToFile(store_file, *this);
}

const QMap<QUrl, Feed *> &MainModel::feeds() const
{
  return _feeds;
}

void MainModel::setFeeds(const QMap<QUrl, Feed *> &feeds)
{
  _feeds = feeds;
  foreach(Feed* f, _feeds.values())
    updateNumUnread(f->url());
}

const QMap<QUrl, Post *> &MainModel::posts() const
{
  return _posts;
}

void MainModel::setPosts(const QMap<QUrl, Post *> &posts)
{
  _posts = posts;
}

FeedsModel *MainModel::feedsModel()
{
  return new FeedsModel(_feeds.values(), this);
}

class PostDateGreater {
public:
  bool operator()(Post* lhs, Post* rhs)const {
    return lhs->pubDate()>rhs->pubDate();
  }
};

PostsModel *MainModel::currentFeed()
{
  if(!_feeds.contains(_state.cur_feed))
    return 0;
  auto f = _feeds.value(_state.cur_feed);
  PostsModel* posts = new PostsModel(this);
  QList<Post*> pts;
  foreach(const QUrl& post_url, f->posts())
    if(_posts.contains(post_url))
      pts.push_back(_posts.value(post_url));
  qSort(pts.begin(), pts.end(), PostDateGreater());
  posts->setPosts(pts);
  return posts;
}

Post *MainModel::currentPost() const
{
  if(!_posts.contains(_state.cur_post))
    return 0;
  return _posts.value(_state.cur_post);
}

QUrl MainModel::currentFeedUrl() const
{
  return _state.cur_feed;
}

QString MainModel::word() const
{
  return _state.word;
}

void MainModel::setWord(const QString &word)
{
  _state.word = word;
  emit sigWordChanged(word);
}

int MainModel::twinkleRate() const
{
  return _twinkle->options().rate;
}

void MainModel::setTwinkleRate(int rate)
{
  auto to = _twinkle->options();
  to.rate = rate;
  _twinkle->setOptions(to);
  emit sigTwinkleOptionsChanged();
}

void MainModel::setTwinkleOptions(const TwinkleOptions &to)
{
  _twinkle->setOptions(to);
}

const Twinkle &MainModel::twinkle() const
{
  return *_twinkle;
}

const QString &MainModel::status() const
{
  return _status;
}

void MainModel::setStatus(const QString &status)
{
  _status = status;
  if(!_status.isEmpty())
    QTimer::singleShot(5000, this, SLOT(slotEmptyStatus()));
  emit sigStatusChanged();
}

void MainModel::updateNumUnread(const QUrl &feed_url)
{
  if(!_feeds.contains(feed_url))
    return;
  Feed* feed = _feeds.value(feed_url);
  int cnt = 0;
  foreach(const QUrl& post_url, feed->posts()) {
    if(!_posts.contains(post_url))
      continue;
    if(!_posts.value(post_url)->isRead())
      ++cnt;
  }
  feed->setNumUnread(cnt);
}

int MainModel::postView() const
{
  return _state.post_view;
}

void MainModel::setPostView(int post_view)
{
  _state.post_view = (ModelState::EPostView)post_view;
  emit sigPostViewChanged();
}

int MainModel::interval() const
{
  return _state.interval;
}

void MainModel::setInterval(int interval)
{
  qDebug()<<"update interval:" <<interval<<" sec";
  _state.interval = interval;
  if(_check_timer->isActive()) {
    _check_timer->stop();
  }
  if(_state.interval>0) {
    _check_timer->setInterval(_state.interval*1000);
    _check_timer->start();
  }
  emit sigIntervalChanged();
}

void MainModel::slotAddPost(Post *post)
{
  Post * p = new Post(*post, this);
  auto it = _feeds.find(p->feed());
  if(it ==_feeds.end())
    return;
  if(_posts.contains(p->url()))
    return;
  _posts.insert(p->url(), p);
  (*it)->addPost(p);
  updateNumUnread((*it)->url());
  setStatus(tr("Updated: ")+post->title());
  if(_tray) {
    _tray->showMessage(tr("New post"), p->title());
    QtQuick2ApplicationViewer* viewer = qobject_cast<QtQuick2ApplicationViewer*>(this->parent());
    viewer->alert(0);
  }
  emit sigUpdateViews();
}

void MainModel::slotAddFeed(const QString &title, const QString &url, const QString&)
{
  QUrl feed_url(url);
  if(_feeds.contains(feed_url)) {
    if(title.isEmpty())
      return;
    Feed* f = _feeds.value(feed_url);
    if(f->title().isEmpty()) {
      setStatus(tr("Update feed title"));
      f->setTitle(title);
    }
  }
  else {
    Feed* feed = new Feed(this);
    feed->setNumUnread(0);
    feed->setTitle(title);
    feed->setUrl(feed_url);
    _feeds.insert(feed_url, feed);
  }
  emit sigUpdateViews();
}

void MainModel::slotFeedSelected(const QUrl& feed_url)
{
  _state.cur_feed = feed_url;
  emit sigCurFeedUrlChanged();
}

void MainModel::slotRemoveFeed(const QUrl & feed_url)
{
  if(!_feeds.contains(feed_url))
    return;
  Feed* f = _feeds.value(feed_url);
  foreach(const QUrl& post_url,f->posts())
    _posts.remove(post_url);
  _feeds.remove(feed_url);
  _state.cur_feed = "";
  _state.cur_post = "";
  emit sigCurFeedUrlChanged();
  emit sigUpdateViews();
}

void MainModel::slotPostSelected(const QUrl & post_url)
{
  if(!_posts.contains(post_url)) {
    _state.cur_post = "";
    qDebug()<<post_url<<" unavailable";
    return;
  }
  _state.cur_post = post_url;
  Post* p = _posts.value(post_url);
  p->setRead(true);
  auto feed_url = p->feed();
  if(_feeds.contains(feed_url)) {
    Feed* f = _feeds.value(feed_url);
    updateNumUnread(f->url());
  }
  _twinkle->populateContent(p->content());
}

void MainModel::slotUpdateFeeds()
{
  FeedPoller * poller = new FeedPoller(this);
  connect(poller, &FeedPoller::sigFinishedLoadingFeed, this, &MainModel::slotAddFeed);
  connect(poller, &FeedPoller::sigFinishedLoadingPost, this, &MainModel::slotAddPost);
  connect(poller, SIGNAL(sigError(QUrl,QString)), this, SLOT(slotError(QUrl, QString)));
  connect(poller, SIGNAL(sigProgress(QUrl,qint64,qint64)), this, SLOT(slotProgress(QUrl, qint64,qint64)));
  poller->poll(_feeds.keys());
  qDebug()<<"Feeds update triggered";
}

void MainModel::slotUpdateCurrentFeed()
{
  FeedPoller* poller = new FeedPoller(this);
  connect(poller, &FeedPoller::sigFinishedLoadingPost, this, &MainModel::slotAddPost);
  poller->poll(_state.cur_feed);
}

void MainModel::slotStartTwinkle()
{
  _twinkle->start();
}

void MainModel::slotStopTwinkle()
{
  _twinkle->stop();
}

void MainModel::slotPauseTwinkle()
{
  _twinkle->pause();
}

void MainModel::slotMarkReadFeed(const QUrl & feed_url)
{
  if(!_feeds.contains(feed_url))
    return;
  Feed*f = _feeds.value(feed_url);
  foreach(const QUrl& p_url, f->posts()) {
    if(!_posts.contains(p_url))
      continue;
    Post*p = _posts.value(p_url);
    p->setRead(true);
  }
  updateNumUnread(f->url());
  emit sigUpdateViews();
}

void MainModel::slotSaveSession(const QString &path)
{
  qDebug()<<"Store to:"<<path;
  foreach(Feed* f, _feeds)
    updateNumUnread(f->url());
  ModelStorer::ToFile(path, *this);
}

void MainModel::slotLoadSession(const QString &path)
{
  ModelStorer::FromFile(path, *this);
  emit sigUpdateViews();
}

void MainModel::slotOpenSource()
{
  if(!_state.cur_post.isValid())
    return;
  if(!_posts.contains(_state.cur_post))
    return;
  QDesktopServices::openUrl(_state.cur_post);
}

void MainModel::slotWordChanged(const QString &word)
{
  setWord(word);
}

void MainModel::slotError(const QUrl & url, const QString &err)
{
  setStatus(url.toString()+": "+err);
}

void MainModel::slotProgress(QUrl url, qint64, qint64)
{
  if(!_feeds.contains(url))
    return;
  QString str = _feeds.value(url)->title();
  setStatus(str);
}

void MainModel::slotEmptyStatus()
{
  setStatus("");
}

void MainModel::slotTrayActivated()
{
  QtQuick2ApplicationViewer* viewer = qobject_cast<QtQuick2ApplicationViewer*>(this->parent());
  if(!viewer)
    return;
  viewer->show();
  //  viewer->raise();
}


ModelState::ModelState(): cur_feed(), cur_post(), word(),post_view(Twinkle), interval(0) {}
