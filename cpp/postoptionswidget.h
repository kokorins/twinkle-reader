#ifndef POSTOPTIONSWIDGET_H
#define POSTOPTIONSWIDGET_H

#include <QtWidgets/QWidget>
#include "postoptions.h"

namespace Ui {
class PostOptionsWidget;
}

class PostOptionsWidget : public QWidget
{
  Q_OBJECT
  
public:
  explicit PostOptionsWidget(QWidget *parent = 0);
  ~PostOptionsWidget();
  void setBackWidget(QWidget* backWidget);
  QWidget* backWidget();
  PostOptions opts()const;
  void setOpts(const PostOptions& opts);
  virtual void keyPressEvent(QKeyEvent *event);
protected slots:
  void chooseFile();
signals:
  void back();
private:
  QWidget* _back;

private:
  Ui::PostOptionsWidget *ui;
};

#endif // POSTOPTIONSWIDGET_H
