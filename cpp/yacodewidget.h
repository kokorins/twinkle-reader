#ifndef YACODEWIDGET_H
#define YACODEWIDGET_H

#include <QStateMachine>
#include <QTimer>
#include <QtWidgets/QWidget>

namespace Ui {
class YaCodeWidget;
}
class QKeyEvent;
class YaCodeWidget;
class OAuthManager;
class LentaManager;
namespace yacode {
class IdleState : public QState {
  Q_OBJECT
public:
  IdleState(YaCodeWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  YaCodeWidget* _wid;
};

class VerifyingState : public QState {
  Q_OBJECT
public:
  VerifyingState(YaCodeWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  YaCodeWidget* _wid;
};

class ErrorState : public QState {
  Q_OBJECT
public:
  ErrorState(YaCodeWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
private:
  YaCodeWidget* _wid;
};

class VerifiedState : public QState {
  Q_OBJECT
public:
  VerifiedState(YaCodeWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
private:
  YaCodeWidget* _wid;
};

class CheckState : public QState {
  Q_OBJECT
public:
  CheckState(YaCodeWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  YaCodeWidget* _wid;
};

class CheckedState : public QState {
  Q_OBJECT
public:
  CheckedState(YaCodeWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  YaCodeWidget* _wid;
};

} //namespace yacode
class YaCodeWidget : public QWidget {
  Q_OBJECT
public:
  explicit YaCodeWidget(QWidget *parent = 0);
  ~YaCodeWidget();
  void setAuth(OAuthManager* oauth, LentaManager* lenta);
  virtual void keyPressEvent(QKeyEvent *);
protected slots:
  void goBack();
  void goFore();
  void requestCode();
  void authProgress(qint64, qint64);
  void checkProgress(qint64, qint64);
  void webProgress(int);
  void cancelVerifying();
  void cancelCheck();
  void setStatus(QString text);
signals:
  void back();
  void fore();
  void status(QString);
signals:
  void error();
  void idle();
  void gotToken();
private:
  friend class yacode::IdleState;
  friend class yacode::ErrorState;
  friend class yacode::VerifiedState;
  friend class yacode::VerifyingState;
  friend class yacode::CheckState;
  friend class yacode::CheckedState;
  QStateMachine _m;
  OAuthManager* _auth;
  LentaManager* _lenta;
  Ui::YaCodeWidget *ui;
};

#endif // YACODEWIDGET_H
