#ifndef FEEDSMANAGER_H
#define FEEDSMANAGER_H

#include <QObject>
#include <QVector>
#include <QStringList>
#include <QFont>

class LentaManager;
class LocalLenta;
class FeedsListModel;

class FeedsManager : public QObject {
  Q_OBJECT
public:
  explicit FeedsManager(QObject *parent = 0);
  void setLenta(LocalLenta*local);
  void populateList(FeedsListModel* model);
  void populateList();
  void setFont(const QFont& font);
protected slots:
  void feedsDownloading(qint64,qint64);
  void postsDownloading(qint64,qint64);
public slots:
  void selectItem(int);
  void goBack();
signals:
  void feedsUpdated();
  void postsUpdated();
  void itemSelected(int);
  void requestPopulate(FeedsListModel* model);
  void back();
  void progress(QString);
private:
  LocalLenta*_local;
  int _val;
  QFont _font;
};

#endif // FEEDSMANAGER_H
