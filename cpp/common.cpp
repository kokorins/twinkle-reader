#include "common.h"
#include <QString>
#include <QObject>
#include <QIODevice>
#include <QDebug>
#include <QDesktopServices>
#include <QFont>
#include <QBuffer>
#include <QPixmap>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLayout>

const QString TwinkleUtils::kCancelText = QObject::tr("&Cancel");
const QString TwinkleUtils::kUpdateText = QObject::tr("&Update");
const QString TwinkleUtils::kPostsText = QObject::tr("&Posts");

void TwinkleUtils::WriteString(QIODevice *io, QString str)
{
  QByteArray data = str.toUtf8();
  int size = data.size();
  io->write((char*)&size, sizeof(size));
  io->write(data);
}

QString TwinkleUtils::ReadString(QIODevice *io)
{
  int size;
  io->read((char*)&size, sizeof(int));
  QByteArray ba = io->read(size);
  return QString::fromUtf8(ba.data());
}

void TwinkleUtils::WriteInt(QIODevice *io, int val)
{
  int size = sizeof(val);
  io->write((char*)&size, sizeof(size));
  io->write((char*)&val, size);
}

int TwinkleUtils::ReadInt(QIODevice *io)
{
  int size;
  io->read((char*)&size, sizeof(int));
  int val;
  io->read((char*)&val, size);
  return val;
}

void TwinkleUtils::WriteFont(QIODevice *io, QFont f)
{
  QString fam = f.family();
  int ps = f.pointSize();
  int w = f.weight();
  bool it = f.italic();
  WriteString(io, fam);
  WriteInt(io, ps);
  WriteInt(io, w);
  WriteInt(io, it);
}

QFont TwinkleUtils::ReadFont(QIODevice *io)
{
  QFont f;
  f.setFamily(ReadString(io));
  f.setPointSize(ReadInt(io));
  f.setWeight(ReadInt(io));
  f.setItalic(ReadInt(io));
  return f;
}

void TwinkleUtils::WritePixmap(QIODevice *io, const QPixmap &p)
{
  QByteArray bytes;
  QBuffer buffer(&bytes);
  buffer.open(QIODevice::WriteOnly);
  p.save(&buffer, "PNG");
  int size = buffer.data().size();
  io->write((char*)&size, sizeof(size));
  io->write(buffer.data());
}

QPixmap TwinkleUtils::ReadPixmap(QIODevice *io)
{
  int size;
  io->read((char*)&size, sizeof(int));
  QByteArray ba = io->read(size);
  QPixmap p;
  p.loadFromData(ba);
  return p;
}

QString TwinkleUtils::DefaultDataPath()
{
  QString res = QStandardPaths::standardLocations(QStandardPaths::DataLocation).front();
  if(!res.endsWith("/") && !res.endsWith("\\"))
    res.append("/");
  res.append("twinkle/");
  return res;
}

QString TwinkleUtils::TokenFile()
{
  return "token.txt";
}

QString TwinkleUtils::FeedsFile()
{
  return "content.twr";
}

QString TwinkleUtils::SessionFile()
{
  return "session.twr";
}

void TwinkleUtils::SetParentPageStyle(QWidget *, QWidget *)
{
//  if(wid && wid->layout() && parent && parent->layout()) {
//    wid->layout()->setMargin(parent->layout()->margin());
//  }
//  if(wid && parent)
//    wid->setFont(parent->font());
}
