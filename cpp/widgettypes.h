#ifndef WIDGETTYPES_H_
#define WIDGETTYPES_H_
#include <QString>
namespace ConnectionType {
enum EProviderType {
  NotConnected,
  Yandex,
  EProviderTypeLen
};
QString ToString(EProviderType type);
enum EConnectionType {
  Summary,
  Internal,
  EConnectionTypeLen
};
QString ToString(EConnectionType type);
int ToKey(EConnectionType type);
}
typedef ConnectionType::EProviderType EProviderType;
typedef ConnectionType::EConnectionType EConnectionType;

namespace ReaderType {
enum EReaderTypes {
  Post,
  Twinkle,
  EReaderTypesLen
};
QString ToString(EReaderTypes type);
}
typedef ReaderType::EReaderTypes EReaderTypes;

#endif // WIDGETTYPES_H_
