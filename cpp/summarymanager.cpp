#include "summarymanager.h"

SummaryManager::SummaryManager(QObject *parent) : QObject(parent) {}

void SummaryManager::setProps(const SummaryProperties &props)
{
  emit sigProperties(props);
}

void SummaryManager::goFore()
{
  emit fore();
}

void SummaryManager::goOptions()
{
  emit options();
}

void SummaryManager::goBack()
{
  emit back();
}
