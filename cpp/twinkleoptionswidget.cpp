#include "twinkleoptionswidget.h"
#include "ui_twinkleoptionswidget.h"
#include "common.h"
#include <QKeyEvent>
#include <QDebug>

TwinkleOptionsWidget::TwinkleOptionsWidget(QWidget *parent) :
  QWidget(parent), _back(0),
  ui(new Ui::TwinkleOptionsWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  connect(ui->back_button, SIGNAL(clicked()), this, SIGNAL(back()));
  for(int backlight=0; backlight<TwinkleOptions::ENoActivityLen;++backlight)
    ui->backlight_combo->addItem(ToString((TwinkleOptions::ENoActivity)backlight));
}

TwinkleOptionsWidget::~TwinkleOptionsWidget()
{
  delete ui;
}

void TwinkleOptionsWidget::setBackWidget(QWidget *back)
{
  _back =  back;
}

QWidget *TwinkleOptionsWidget::backWidget()
{
  return _back;
}

TwinkleOptions TwinkleOptionsWidget::opts() const
{
  TwinkleOptions to;
  to.font = ui->font_combo->currentFont();
  to.font_size = ui->font_spin->value();
  to.font.setPointSize(to.font_size);
  to.interval = ui->interval_spin->value();
  to.no_activity = (TwinkleOptions::ENoActivity)ui->backlight_combo->currentIndex();
  return to;
}

void TwinkleOptionsWidget::setOpts(const TwinkleOptions& opts)
{
  ui->font_combo->setCurrentFont(opts.font);
  ui->font_spin->setValue(opts.font_size);
  ui->interval_spin->setValue(opts.interval);
  ui->backlight_combo->setCurrentIndex((int)opts.no_activity);
}

void TwinkleOptionsWidget::keyPressEvent(QKeyEvent *event)
{
  if(event->key()==Qt::Key_Context2) {
    qDebug()<<"context2 emulated";
    ui->back_button->click();
    event->accept();
  }
  else {
    event->ignore();
  }
}
