#ifndef FEEDSWIDGET_H
#define FEEDSWIDGET_H

#include <QtWidgets/QWidget>
#include <QStateMachine>

namespace Ui {
class FeedsWidget;
}

class LentaManager;
class FeedsWidget;
class LocalLenta;
class FeedsListModel;

namespace feeds {
class IdleState : public QState {
  Q_OBJECT
public:
  IdleState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  FeedsWidget* _wid;
};

class EmptyState : public QState {
  Q_OBJECT
public:
  EmptyState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent* event);
private:
  FeedsWidget* _wid;
};

class UpdatingState : public QState {
  Q_OBJECT
public:
  UpdatingState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  FeedsWidget* _wid;
};

class CancelState : public QState {
  Q_OBJECT
public:
  CancelState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
private:
  FeedsWidget* _wid;
};

class UpdatedState : public QState {
  Q_OBJECT
public:
  UpdatedState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
private:
  FeedsWidget* _wid;
};

class CheckFeedState : public QState {
  Q_OBJECT
public:
  CheckFeedState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  FeedsWidget* _wid;
};

class UpdatedFeedState : public QState {
  Q_OBJECT
public:
  UpdatedFeedState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
private:
  FeedsWidget* _wid;
};

class CancelFeedState : public QState {
  Q_OBJECT
public:
  CancelFeedState(FeedsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
private:
  FeedsWidget* _wid;
};
} //namespace feeds

class FeedsManager;
class FeedsWidget : public QWidget {
  Q_OBJECT
public:
  explicit FeedsWidget(QWidget *parent = 0);
  ~FeedsWidget();
  void setLenta(LentaManager* lenta, LocalLenta*local);
  void setManager(FeedsManager * manager);
public:
  virtual void keyPressEvent(QKeyEvent *);
  virtual void setVisible(bool visible);
public slots:
  void setStatus(QString);
protected slots:
  void goBack();
  void selectCurItem();
  void populateList(FeedsListModel*);
signals:
  void fore();
  void emptyList();
  void idle();
  void status(QString);
private:
  FeedsManager * _manager;
  friend class feeds::IdleState;
  friend class feeds::EmptyState;
  friend class feeds::UpdatingState;
  friend class feeds::CancelState;
  friend class feeds::UpdatedState;
  friend class feeds::CancelFeedState;
  friend class feeds::UpdatedFeedState;
  friend class feeds::CheckFeedState;
  QStateMachine _m;
  Ui::FeedsWidget *ui;
};

#endif // FEEDSWIDGET_H
