#ifndef COMMON_H
#define COMMON_H

class QKeyEvent;
class QString;
class QIODevice;
class QFont;
class QWidget;
class QPixmap;

/**
 *  \brief Additional utils funcitons are stored here
 *  The application uses binary format for internal data.
 *  If it needs to save some object into a file, we use the notation:
 *  [objectsize+objectbytes] for primitive types,
 *  and ordered list of children for complex objects.
 */
class TwinkleUtils{
public:
  static const QString kCancelText;
  static const QString kUpdateText;
  static const QString kPostsText;
  static void WriteString(QIODevice* io, QString str);
  static QString ReadString(QIODevice* io);
  static void WriteInt(QIODevice* io, int val);
  static int ReadInt(QIODevice* io);
  static void WriteFont(QIODevice* io, QFont f);
  static QFont ReadFont(QIODevice* io);
  static void WritePixmap(QIODevice*io, const QPixmap& p);
  static QPixmap ReadPixmap(QIODevice*io);
  static QString DefaultDataPath();
  static QString TokenFile();
  static QString FeedsFile();
  static QString SessionFile();
  static void SetParentPageStyle(QWidget *wid, QWidget *parent);
  template <typename T>
  static void ContextKeysHandling(QKeyEvent* event, T* ui);
};

#endif // COMMON_H
