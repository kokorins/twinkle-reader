#include "feedswidget.h"
#include "ui_feedswidget.h"
#include "lentamanager.h"
#include "feedsmanager.h"
#include "locallenta.h"
#include "common.hpp"
#include "lentatypes.h"
#include "lentatypes.h"
#include <QTimer>
#include <QKeyEvent>
#include <QtWidgets/QListWidgetItem>
#include <QDebug>

FeedsWidget::FeedsWidget(QWidget *parent) : QWidget(parent), ui(new Ui::FeedsWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  connect(ui->fore_button, SIGNAL(clicked()), this, SIGNAL(fore()));
  connect(ui->list, SIGNAL(activated(QModelIndex)), this, SIGNAL(fore()));
  setStatus("");
}

FeedsWidget::~FeedsWidget()
{
  delete ui;
}

void FeedsWidget::setManager(FeedsManager *manager)
{
  _manager = manager;
  _manager->setFont(font());
  connect(_manager, SIGNAL(requestPopulate(FeedsListModel*)), this, SLOT(populateList(FeedsListModel*)));
  using namespace feeds;
  QState* idle = new IdleState(this);
  QState* empty = new EmptyState(this);
  QState* updating = new UpdatingState(this);
  QState* cancel = new CancelState(this);
  QState* updated = new UpdatedState(this);
  QState* check_feed = new CheckFeedState(this);
  QState* cancel_feed = new CancelFeedState(this);
  QState* updated_feed = new UpdatedFeedState(this);
  idle->addTransition(this, SIGNAL(emptyList()), empty);
  idle->addTransition(this, SIGNAL(fore()), check_feed);
  idle->addTransition(_manager, SIGNAL(feedsUpdated()), updated);
  empty->addTransition(_manager, SIGNAL(feedsUpdated()), updated);
  updating->addTransition(_manager, SIGNAL(feedsUpdated()), updated);
  cancel->addTransition(this, SIGNAL(idle()), idle);
  updated->addTransition(this, SIGNAL(idle()), idle);
  check_feed->addTransition(_manager, SIGNAL(postsUpdated()), updated_feed);
  check_feed->addTransition(this, SIGNAL(fore()), cancel_feed);
  cancel_feed->addTransition(this, SIGNAL(idle()), idle);
  updated_feed->addTransition(this, SIGNAL(idle()), idle);
  _m.addState(idle);
  _m.addState(empty);
  _m.addState(updating);
  _m.addState(cancel);
  _m.addState(updated);
  _m.addState(check_feed);
  _m.addState(cancel_feed);
  _m.addState(updated_feed);
  _m.setInitialState(idle);
  _m.start();
}

void FeedsWidget::populateList(FeedsListModel* model)
{
  QItemSelectionModel *m = ui->list->selectionModel();
  ui->list->setModel(model);
  if(m)
    m->deleteLater();
  ui->list->setWordWrap(true);
}

void FeedsWidget::keyPressEvent(QKeyEvent * event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}

void FeedsWidget::setVisible(bool visible)
{
  QWidget::setVisible(visible);
  if(visible) {
    QAbstractItemModel * model = ui->list->model();
    if(!model || model->rowCount()>0)
      ui->list->setFocus();
  }
}

void FeedsWidget::setStatus(QString text)
{
  ui->status->setText(text);
  ui->status->setVisible(!text.isEmpty());
}

void FeedsWidget::goBack()
{
  _manager->goBack();
}

void FeedsWidget::selectCurItem()
{
  _manager->selectItem(ui->list->currentIndex().row());
}

namespace feeds {
void IdleState::onEntry(QEvent *)
{  
  qDebug()<<"feeds:idle";
  QAbstractItemModel * model = _wid->ui->list->model();
  if(!model || model->rowCount()<1) {
    emit _wid->emptyList();
    return;
  }
  if(_wid->ui->list->isVisible())
    _wid->ui->list->setFocus();
  connect(_wid->ui->back_button, SIGNAL(clicked()), _wid, SLOT(goBack()));
  _wid->ui->fore_button->setEnabled(true);
  _wid->ui->fore_button->setText(tr("&Posts"));
//  _wid->ui->update_button->setText(tr("&Update feed"));
}

void IdleState::onExit(QEvent *)
{
  disconnect(_wid->ui->back_button, SIGNAL(clicked()), _wid, SLOT(goBack()));
}

void EmptyState::onEntry(QEvent *)
{
  qDebug()<<"feeds:empty";
  connect(_wid->ui->back_button, SIGNAL(clicked()), _wid, SLOT(goBack()));
  _wid->ui->fore_button->setEnabled(false);
//  _wid->ui->update_button->setText("&Update feeds");
}

void EmptyState::onExit(QEvent *)
{
  disconnect(_wid->ui->back_button, SIGNAL(clicked()), _wid, SLOT(goBack()));
}

void UpdatingState::onEntry(QEvent *)
{
  qDebug()<<"feeds:updating";
  connect(_wid->_manager, SIGNAL(progress(QString)), _wid, SIGNAL(status(QString)));
  _wid->ui->fore_button->setEnabled(false);
}

void UpdatingState::onExit(QEvent *)
{
  disconnect(_wid->_manager, SIGNAL(progress(QString)), _wid, SIGNAL(status(QString)));
}

void CancelState::onEntry(QEvent *)
{
  emit _wid->idle();
}

void UpdatedState::onEntry(QEvent *)
{
  _wid->_manager->populateList();
  emit _wid->idle();
}

void CheckFeedState::onEntry(QEvent *)
{
  qDebug()<<"feeds:request posts";
  connect(_wid->_manager, SIGNAL(progress(QString)), _wid, SIGNAL(status(QString)));
  _wid->ui->fore_button->setText(TwinkleUtils::kCancelText);
  _wid->selectCurItem();
}

void CheckFeedState::onExit(QEvent *)
{
  _wid->ui->fore_button->setText(TwinkleUtils::kPostsText);
  disconnect(_wid->_manager, SIGNAL(progress(QString)), _wid, SIGNAL(status(QString)));
}

void UpdatedFeedState::onEntry(QEvent *)
{
  emit _wid->idle();
}

void CancelFeedState::onEntry(QEvent *)
{
  emit _wid->idle();
}
}//namespace feeds
