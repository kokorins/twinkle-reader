#include "locallenta.h"
#include "common.h"
#include <QFile>
#include <QDir>
#include <QDebug>

LocalLenta::LocalLenta(QObject *parent) : QObject(parent), _lock(QReadWriteLock::Recursive) {}

SummaryProperties LocalLenta::summary()
{
  QReadLocker locker(& _lock);
  return _summary;
}

void LocalLenta::setSummary(const SummaryProperties &summary)
{
  QWriteLocker locker(&_lock);
  _summary = summary;
  emit summaryChanged();
}

void LocalLenta::setFeeds(const QVector<FeedInfo> &feeds)
{
  QWriteLocker locker(&_lock);
  _feeds = feeds;
  emit feedsChanged();
}

void LocalLenta::updateFeed(int idx, const FeedInfo &fi)
{
  QWriteLocker locker(&_lock);
  _feeds[idx] = fi;
  emit feedsChanged();
}

FeedInfo LocalLenta::feed(int idx)
{
  QReadLocker locker(&_lock);
  return _feeds[idx];
}

int LocalLenta::searchFeed(FeedInfo::EFeedField field, const QString &val)
{
  QReadLocker locker(&_lock);
  int res = -1;
  for(int i=0; i<_feeds.size(); ++i) {
    const FeedInfo& fi = _feeds[i];
    switch(field) {
    break; case FeedInfo::MD5:
      if(fi.md5==val)
        res = i;
    break; case FeedInfo::Title:
      if(fi.title==val)
        res = i;
    break; case FeedInfo::Link:
      if(fi.link == val)
        res = i;
    break; case FeedInfo::Unread:
      if(fi.unread == val.toInt())
        res = i;
    break; case FeedInfo::FavUrl:
      if(fi.fav_url==val)
        res = i;
    break; case FeedInfo::Fav:
      res = -1;
    break; default:
      res = -1;
    }
  }
  return res;
}

QVector<FeedInfo> LocalLenta::feeds()
{
  QReadLocker locker(&_lock);
  return _feeds;
}

QStringList LocalLenta::posts(QString feed_md5)
{
  QReadLocker locker(&_lock);
  QStringList posts;
  foreach(const QString& k, _posts.keys())
    if(_posts.value(k).md5 == feed_md5)
      posts.push_back(_posts.value(k).id);
  return posts;
}

PostInfo LocalLenta::post(QString id)
{
  QReadLocker locker(&_lock);
  return _posts[id];
}

void LocalLenta::setPost(QString id, const PostInfo &post)
{
  QWriteLocker locker(&_lock);
  _posts[id] = post;
  emit postChanged(id);
}

bool LocalLenta::checkPosts(QString feed_md5)
{
  QReadLocker locker(&_lock);
  foreach(const QString&k, _posts.keys())
    if(_posts.value(k).md5 == feed_md5)
      return true;
  return false;
}

bool LocalLenta::save(QString path, QString name)
{
  QDir dir(path);
  if(!dir.exists(path))
    dir.mkpath(path);
  QFile file(path+name);
  if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate))
    return false;
  QReadLocker locker(&_lock);
  TwinkleUtils::WriteInt(&file, kVersion);
  _summary.save(&file);
  TwinkleUtils::WriteInt(&file, _feeds.size());
  for(int i=0; i<_feeds.size(); ++i)
    _feeds[i].save(&file);
  TwinkleUtils::WriteInt(&file, _posts.size());
  const QList<QString>& keys = _posts.keys();
  for(int i=0; i<keys.size(); ++i) {
    TwinkleUtils::WriteString(&file, keys[i]);
    _posts[keys[i]].save(&file);
  }
  return true;
}

bool LocalLenta::load(QString path)
{
  QFile file(path);
  if (!file.open(QIODevice::ReadOnly))
    return false;
  QWriteLocker locker(&_lock);
  clear();
  int ver = TwinkleUtils::ReadInt(&file);
  if(ver!=kVersion)
    return false;
  _summary.load(&file);
  int feeds_size = TwinkleUtils::ReadInt(&file);
  for(int i=0; i<feeds_size; ++i) {
    FeedInfo fi;
    fi.load(&file);
    _feeds.push_back(fi);
  }
  int posts_size = TwinkleUtils::ReadInt(&file);
  for(int i=0; i<posts_size; ++i) {
    QString key = TwinkleUtils::ReadString(&file);
    PostInfo pi;
    pi.load(&file);
    _posts[key] = pi;
  }
  return true;
}

void LocalLenta::clear()
{
  QWriteLocker locker(&_lock);
  _feeds.clear();
  _posts.clear();
}
