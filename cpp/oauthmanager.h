#ifndef OAUTHMANAGER_H
#define OAUTHMANAGER_H

#include <QObject>
#include <QString>
#include <QDateTime>
#include <QMap>
#include <QNetworkReply>
#include <QSslError>


class OAuthManager : public QObject
{
  Q_OBJECT
public:
  enum ERequestType {
    Code,
    Token,
    ERequestTypeLen
  };
public:
  explicit OAuthManager(QObject *parent = 0);
  bool requestCode();
  bool requestToken();
  bool hasToken()const;
  bool isExpired()const;
  QString token()const;
  QDateTime expiration()const;
  QString code()const;
  void setToken(QString token, QDateTime expire);
  void setCode(QString code);
  void setIdPass(QString id, QString _pass);
  bool restore(QString file_name);
  void dump(QString file_name);
  QUrl codeUrl()const;
  QUrl tokenUrl()const;
  void setRequestType(ERequestType tp);
public slots:
  void cancel();
  void acquireToken();
  void acquireTokenError(QNetworkReply::NetworkError);
  void acqureTokenProgress(qint64,qint64);
  void sslErrors(QList<QSslError>);
signals:
  void status(QString);
  void replyError();
  void replyProgress(qint64,qint64);
  void replyFinished();
private:
  static bool ParseTokenJson(QString token_str, QMap<QString,QString>& res);
private:
  QNetworkReply *_reply;
  ERequestType _req_type;
  bool _has_id;
  QString _id;
  QString _pass;
  QString _token;
  QDateTime _expire;
  QString _code;
  bool _has_token;
  bool _has_code;
};

#endif // OAUTHMANAGER_H
