#include "yacodewidget.h"
#include "ui_yacodewidget.h"
#include "oauthmanager.h"
#include "lentamanager.h"
#include "common.hpp"
#include <QDebug>
#include <QKeyEvent>
#include <QtWidgets/QWidget>

YaCodeWidget::YaCodeWidget(QWidget *parent) : QWidget(parent), ui(new Ui::YaCodeWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  connect(ui->back_button, SIGNAL(clicked()), this, SLOT(goBack()));
  connect(ui->web, SIGNAL(loadProgress(int)), this, SLOT(webProgress(int)));
  setStatus("");
}

YaCodeWidget::~YaCodeWidget()
{
  delete ui;
}

void YaCodeWidget::setAuth(OAuthManager *oauth, LentaManager* lenta)
{
  _auth = oauth;
  _lenta = lenta;
  QState * idle = new yacode::IdleState(this);
  QState * verifying = new yacode::VerifyingState(this);
  QState * error = new yacode::ErrorState(this);
  QState * verified = new yacode::VerifiedState(this);
  QState * checking = new yacode::CheckState(this);
  QState * checked = new yacode::CheckedState(this);
  idle->addTransition(ui->fore_button, SIGNAL(clicked()), verifying);
  verifying->addTransition(_auth, SIGNAL(replyFinished()), verified);
  verifying->addTransition(_auth, SIGNAL(replyError()), error);
  verifying->addTransition(this, SIGNAL(idle()), idle);
  error->addTransition(this, SIGNAL(idle()), idle);
  verified->addTransition(this, SIGNAL(error()), error);
  verified->addTransition(this, SIGNAL(gotToken()), checking);
  checking->addTransition(this, SIGNAL(idle()), idle);
  checking->addTransition(_lenta, SIGNAL(summaryUpdated()), checked);
  checking->addTransition(_lenta, SIGNAL(connectionError()), error);
  checked->addTransition(ui->code_button, SIGNAL(clicked()), idle);
  _m.addState(idle);
  _m.addState(verifying);
  _m.addState(error);
  _m.addState(verified);
  _m.addState(checking);
  _m.addState(checked);
  _m.setInitialState(idle);
  _m.start();
}

void YaCodeWidget::keyPressEvent(QKeyEvent * event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}

void YaCodeWidget::goBack()
{
  emit back();
}

void YaCodeWidget::goFore()
{
  emit fore();
}

void YaCodeWidget::requestCode()
{
  QUrl url(_auth->codeUrl());
  if(url.isEmpty()) {
    emit status(tr("No id"));
    emit error();
    return;
  }
  ui->web->load(url);
}

void YaCodeWidget::authProgress(qint64 cur, qint64 tot)
{
  emit status(tr("auth %1/%2").arg(cur).arg(tot));
}

void YaCodeWidget::checkProgress(qint64 cur, qint64 tot)
{
  emit status(tr("check %1/%2").arg(cur).arg(tot));
}

void YaCodeWidget::webProgress(int prog)
{
  qDebug()<<ui->web->url();
  if(ui->web->url()!=QUrl("about:blank"))
    emit status(tr("Loading... %1%").arg(prog));
}

void YaCodeWidget::cancelVerifying()
{
  _auth->cancel();
  emit status(tr("Canceled verification"));
  emit idle();
}

void YaCodeWidget::cancelCheck()
{
  _lenta->cancel(LentaManager::SN);
  _lenta->cancel(LentaManager::SU);
  emit status(tr("Canceled check"));
  emit idle();
}

void YaCodeWidget::setStatus(QString text)
{
  ui->status->setText(text);
  ui->status->setVisible(!text.isEmpty());
}

namespace yacode {
void IdleState::onEntry(QEvent *)
{
  _wid->ui->fore_button->setText(tr("&Verify"));
  connect(_wid->ui->code_button, SIGNAL(clicked()), _wid, SLOT(requestCode()));
  _wid->ui->web->load(QUrl("about:blank"));
}

void IdleState::onExit(QEvent *)
{
  disconnect(_wid->ui->code_button, SIGNAL(clicked()), _wid, SLOT(requestCode()));
}

void VerifyingState::onEntry(QEvent *)
{
  qDebug()<<"Verifying";
  connect(_wid->_auth, SIGNAL(replyProgress(qint64,qint64)), _wid, SLOT(authProgress(qint64, qint64)));
  _wid->ui->fore_button->setText(tr("&Cancel"));
  connect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SLOT(cancelVerifying()));
  _wid->_auth->setCode(_wid->ui->code_line->text());
  _wid->_auth->requestToken();
}

void VerifyingState::onExit(QEvent *)
{
  disconnect(_wid->_auth, SIGNAL(replyProgress(qint64,qint64)), _wid, SLOT(authProgress(qint64, qint64)));
  disconnect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SLOT(cancelVerifying()));
}

void ErrorState::onEntry(QEvent *)
{
  emit _wid->status(tr("Error"));
  emit _wid->idle();
}

void VerifiedState::onEntry(QEvent *)
{
  qDebug()<<"Verified";
  if(!_wid->_auth->hasToken()) {
    emit _wid->status(tr("Incorrect token"));
    emit _wid->error();
  }
  else {
    _wid->_auth->dump("token.txt");
    _wid->ui->code_line->setText("");
    emit _wid->gotToken();
  }
}

void CheckState::onEntry(QEvent *)
{
  qDebug()<<"yacode:checking";
  _wid->ui->fore_button->setText(tr("&Cancel"));
  connect(_wid->_lenta, SIGNAL(summaryDownloading(qint64,qint64)), _wid, SLOT(checkProgress(qint64,qint64)));
  connect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SLOT(cancelCheck()));
  _wid->_lenta->requestSummary();
}

void CheckState::onExit(QEvent *)
{
  _wid->ui->fore_button->setText(tr("&Verify"));
  disconnect(_wid->_lenta, SIGNAL(summaryDownloading(qint64,qint64)), _wid, SLOT(checkProgress(qint64,qint64)));
  disconnect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SLOT(cancelCheck()));
}

void CheckedState::onEntry(QEvent *)
{
  qDebug()<<"checked";
  emit _wid->status(tr("Token checked"));
  _wid->ui->fore_button->setText(tr("&Summary"));
  connect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SLOT(goFore()));
}

void CheckedState::onExit(QEvent *)
{
  disconnect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SLOT(goFore()));
}
}
