#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "oauthmanager.h"
#include "lentamanager.h"
#include "locallenta.h"
#include "widgettypes.h"
#include "common.h"
#include "connectionwidget.h"
#include "connectionmanager.h"
#include "yacodewidget.h"
#include "summarywidget.h"
#include "summarymanager.h"
#include "feedsmanager.h"
#include "feedswidget.h"
#include "postsmanager.h"
#include "postswidget.h"
#include "postwidget.h"
#include "twinklewidget.h"
#include "postoptionswidget.h"
#include "optionswidget.h"
#include "twinkleoptionswidget.h"
#include "mainoptions.h"
#include <QtCore/QCoreApplication>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QDebug>
#include <QDomDocument>
#include <QDesktopServices>
#include <QProcess>
#include <QKeyEvent>
#include <algorithm>
#include <QFile>
#include <QTextStream>
#include <QXmlReader>
#include <QTextStream>
#include <QDesktopServices>
#include <QDir>

const QString MainWindow::_id = "8c1afc77f3934b2893ed20001e36bca5";
const QString MainWindow::_pass = "554e65e7141d4bd58aa9062608a8adfa";
const QString MainWindow::kLentaUrl = "https://api-lenta.yandex.ru";

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
  ui->setupUi(this);
  _progress_timer = new QTimer(this);
  _progress_timer->setSingleShot(true);
  connect(_progress_timer, SIGNAL(timeout()), this, SLOT(hideProgress()));
  setStatusText("");
  _opts = new MainOptions(this);
  if(QFile::exists(TwinkleUtils::DefaultDataPath()+TwinkleUtils::SessionFile()))
    _opts->load(TwinkleUtils::DefaultDataPath()+TwinkleUtils::SessionFile());
  if(!ui->stackedWidget->layout())
    ui->stackedWidget->setLayout(new QVBoxLayout(ui->stackedWidget));
  ui->stackedWidget->layout()->setMargin(_opts->margin);
  QFont f = ui->stackedWidget->font();
  f.setPointSize(_opts->font_size);
  ui->stackedWidget->setFont(f);

  setupLogin();
  ui->stackedWidget->setCurrentWidget(conn_widget);
  setupAuth();
  setupLocalLenta();
  setupLenta();

  setupYaCode();
  //summary
  setupSummary();
  //feeds
  setupFeeds();
  //posts
  setupPosts();
  //post
  setupPost();
  //twinkle
  setupTwinkle();
  //options
  setupOptions();
  //post options
  setupPostOptions();
  //twinkle options
  setupTwinkleOptions();
}

void MainWindow::setupLogin()
{
  _conn_manager = new ConnectionManager(this);
  conn_widget = new ConnectionWidget(_opts, ui->stackedWidget);
  conn_widget->setManager(_conn_manager);
  ui->stackedWidget->addWidget(conn_widget);
  QStringList login_list;
  for(int i=0; i<ConnectionType::EConnectionTypeLen; ++i)
    login_list += ConnectionType::ToString((EConnectionType)i);
  _conn_manager->populateList(login_list);
  connect(_conn_manager, SIGNAL(back()), this, SLOT(close()));
  connect(_conn_manager, SIGNAL(itemSelected(int)), this, SLOT(changeConnType(int)));
  connect(conn_widget, SIGNAL(status(QString)), this, SLOT(setStatusText(QString)));
  connect(this, SIGNAL(status(QString)), conn_widget, SLOT(setStatus(QString)));
}

void MainWindow::setupAuth()
{
  _auth = new OAuthManager(this);
  _auth->setIdPass(_id, _pass);
  connect(_auth, SIGNAL(replyError()), this, SLOT(handleAuthError()));
  connect(_auth, SIGNAL(status(QString)), this, SLOT(setStatusText(QString)));
  if(!_auth->restore(TwinkleUtils::TokenFile())) {
    setStatusText(tr("No previous token"));
  }
  else {
    setStatusText(tr("Yandex token restored"));
  }
}

void MainWindow::setupLenta()
{
  _lenta = new LentaManager(_auth->token(), this);
  _lenta->setLocal(_local);
  connect(_lenta, SIGNAL(connectionError()), this, SLOT(connectionChange()));
}

void MainWindow::setupOptions()
{
  options_widget = new OptionsWidget(ui->stackedWidget);
  options_widget->init(_opts->rt);
  connect(options_widget, SIGNAL(fore()), this, SLOT(showSpecialOptions()));
  connect(options_widget, SIGNAL(back()), this, SLOT(returnGeneralOptions()));
  connect(options_widget, SIGNAL(typeChanged(int)), this, SLOT(changePostType(int)));
  ui->stackedWidget->addWidget(options_widget);
}

void MainWindow::setupYaCode()
{
  ya_code_widget = new YaCodeWidget(ui->stackedWidget);
  ya_code_widget->setAuth(_auth, _lenta);
  connect(ya_code_widget, SIGNAL(back()), this, SLOT(showConnection()));
  connect(ya_code_widget, SIGNAL(fore()), this, SLOT(showSummary()));
  connect(ya_code_widget, SIGNAL(status(QString)), this, SLOT(setStatusText(QString)));
  connect(this, SIGNAL(status(QString)), ya_code_widget, SLOT(setStatus(QString)));
  ui->stackedWidget->addWidget(ya_code_widget);
}

void MainWindow::setupSummary()
{
  _sum_manager = new SummaryManager(this);
  summary_widget = new SummaryWidget(ui->stackedWidget);
  summary_widget->setManager(_sum_manager);
  ui->stackedWidget->addWidget(summary_widget);
  _sum_manager->setProps(_local->summary());
  connect(_sum_manager, SIGNAL(back()), this, SLOT(showConnection()));
  connect(_sum_manager, SIGNAL(fore()), this, SLOT(showFeeds()));
  connect(_sum_manager, SIGNAL(options()), this, SLOT(showOptions()));
  connect(_sum_manager, SIGNAL(update()), this, SLOT(updateSummary()));
  connect(summary_widget, SIGNAL(status(QString)), this, SLOT(setStatusText(QString)));
  connect(this, SIGNAL(status(QString)), summary_widget, SLOT(setStatus(QString)));
}

void MainWindow::setupFeeds()
{
  _feeds_manager = new FeedsManager(this);
  _feeds_manager->setLenta(_local);
  feeds_widget = new FeedsWidget(ui->stackedWidget);
  feeds_widget->setManager(_feeds_manager);
  ui->stackedWidget->addWidget(feeds_widget);
  _feeds_manager->populateList();
  connect(_feeds_manager, SIGNAL(back()), this, SLOT(showSummary()));
  connect(_feeds_manager, SIGNAL(itemSelected(int)), this, SLOT(updatePosts(int)));
  connect(_feeds_manager, SIGNAL(progress(QString)), this, SLOT(setStatusText(QString)));
  connect(feeds_widget, SIGNAL(status(QString)), this, SLOT(setStatusText(QString)));
}

void MainWindow::setupPosts()
{
  _posts_manager = new PostsManager(this);
  _posts_manager->setLenta(_lenta, _local);
  posts_widget = new PostsWidget(ui->stackedWidget);
  posts_widget->setManager(_posts_manager);
  ui->stackedWidget->addWidget(posts_widget);
  connect(_posts_manager, SIGNAL(back()), this, SLOT(showFeeds()));
  connect(_posts_manager, SIGNAL(itemSelected(int)), this, SLOT(showPost(int)));
  connect(posts_widget, SIGNAL(status(QString)), this, SLOT(setStatusText(QString)));
}

void MainWindow::setupPost()
{
  post_widget = new PostWidget(ui->stackedWidget);
  post_widget->setOpts(_opts->po);
  connect(post_widget, SIGNAL(back()), this, SLOT(showPosts()));
  connect(post_widget, SIGNAL(fore()), this, SLOT(showOptions()));
  connect(post_widget, SIGNAL(requestSource()), this, SLOT(openSource()));
  ui->stackedWidget->addWidget(post_widget);
}

void MainWindow::setupPostOptions()
{
  post_options_widget = new PostOptionsWidget(ui->stackedWidget);
  post_options_widget->setOpts(_opts->po);
  connect(post_options_widget, SIGNAL(back()), this, SLOT(returnPostOptions()));
  ui->stackedWidget->addWidget(post_options_widget);
}

void MainWindow::setupTwinkle()
{
  twinkle_widget = new TwinkleWidget(ui->stackedWidget);
  twinkle_widget->setTitle(tr("Post"));
  twinkle_widget->setOpts(_opts->to);
  connect(twinkle_widget, SIGNAL(back()), this, SLOT(showPosts()));
  connect(twinkle_widget, SIGNAL(fore()), this, SLOT(showOptions()));
  connect(twinkle_widget, SIGNAL(requestSource()), this, SLOT(openSource()));
  ui->stackedWidget->addWidget(twinkle_widget);
}

void MainWindow::setupTwinkleOptions()
{
  twinkle_options_widget = new TwinkleOptionsWidget(ui->stackedWidget);
  twinkle_options_widget->setOpts(_opts->to);
  connect(twinkle_options_widget, SIGNAL(back()), this, SLOT(returnTwinkleOptions()));
  ui->stackedWidget->addWidget(twinkle_options_widget);
}

void MainWindow::setupLocalLenta()
{
  _local = new LocalLenta(this);
  if(QFile::exists(TwinkleUtils::DefaultDataPath()+TwinkleUtils::FeedsFile()))
    _local->load(TwinkleUtils::DefaultDataPath()+TwinkleUtils::FeedsFile());
}

MainWindow::~MainWindow()
{
  delete ui;
}

QWidget *MainWindow::readerWidget(EReaderTypes type)
{
  switch(type) {
  break; case ReaderType::Post:
    return post_widget;
  break; case ReaderType::Twinkle:
    return twinkle_widget;
  break; default:
    return 0;
  }
}

void MainWindow::updateSummary()
{
  _sum_manager->setProps(_local->summary());
}

void MainWindow::showSummary()
{
  ui->stackedWidget->setCurrentWidget(summary_widget);
}

void MainWindow::updatePosts(int idx)
{
  _posts_manager->setCurrentFeed(idx);
  ui->stackedWidget->setCurrentWidget(posts_widget);
}

void MainWindow::showFeeds()
{
  ui->stackedWidget->setCurrentWidget(feeds_widget);
}

void MainWindow::showPosts()
{
  ui->stackedWidget->setCurrentWidget(posts_widget);
}

void MainWindow::showPost(int idx)
{
  _cur_post = idx;
  const PostInfo& pi = _local->post(_posts_manager->currentPosts()[_cur_post]);
  if(!pi.read)
    _lenta->requestMarkRead(pi.id);
  post_widget->setTitle(pi.title);
  QString stylesheet;
  if(!_opts->po.stylesheetPath().isEmpty())
    stylesheet = "<link rel=\"stylesheet\" type=\"text/css\" href=\""+_opts->po.stylesheetPath()+"\"/>";
  QString xml = "<html>"+stylesheet+"<body>"+pi.content+"</body></html>";
  post_widget->populatePost(xml);
  twinkle_widget->setTitle(pi.title);
  QDomDocument dd;
  if(!dd.setContent(xml))
    qDebug()<<"Cant set content xml";
  twinkle_widget->setText(dd.documentElement().text());
  switch(_opts->rt) {
  break; case ReaderType::Post:
    ui->stackedWidget->setCurrentWidget(post_widget);
  break; case ReaderType::Twinkle:
    ui->stackedWidget->setCurrentWidget(twinkle_widget);
  break; default:
    ;
  }
}

void MainWindow::showOptions()
{
  options_widget->setBackWidget(ui->stackedWidget->currentWidget());
  ui->stackedWidget->setCurrentWidget(options_widget);
}

void MainWindow::changePostType(int type)
{
  if(options_widget->backWidget()==readerWidget(_opts->rt))
    options_widget->setBackWidget(readerWidget((ReaderType::EReaderTypes)type));
  _opts->rt = (ReaderType::EReaderTypes)type;
}

void MainWindow::showPostOptions()
{
  post_options_widget->setBackWidget(ui->stackedWidget->currentWidget());
  ui->stackedWidget->setCurrentWidget(post_options_widget);
}

void MainWindow::showTwinkleOptions()
{
  twinkle_options_widget->setBackWidget(ui->stackedWidget->currentWidget());
  ui->stackedWidget->setCurrentWidget(twinkle_options_widget);
}

void MainWindow::showSpecialOptions()
{
  switch(_opts->rt) {
  break;case ReaderType::Post: {
    showPostOptions();
  }
  break;case ReaderType::Twinkle: {
    showTwinkleOptions();
  }
  break; default:
    ;
  }
}

void MainWindow::changeConnType(int value)
{
  switch((EConnectionType)value) {
  break; case ConnectionType::Summary:
    ui->stackedWidget->setCurrentWidget(summary_widget);
  break; case ConnectionType::Internal:
    ui->stackedWidget->setCurrentWidget(ya_code_widget);
  break; default:
      ;
  }
}

void MainWindow::setStatusText(QString text)
{
  emit status(text);
  if(!text.isEmpty())
    _progress_timer->start(_opts->progress_timout_msec);
}

void MainWindow::showConnection()
{
  ui->stackedWidget->setCurrentWidget(conn_widget);
}

void MainWindow::returnTwinkleOptions()
{
  _opts->to = twinkle_options_widget->opts();
  twinkle_widget->setOpts(_opts->to);
  if(!twinkle_options_widget->backWidget())
    showOptions();
  else
    ui->stackedWidget->setCurrentWidget(twinkle_options_widget->backWidget());
}

void MainWindow::returnPostOptions()
{
  _opts->po = post_options_widget->opts();
  post_widget->setOpts(_opts->po);
  const PostInfo& pi = _local->post(_posts_manager->currentPosts()[_cur_post]);
  if(!pi.read)
    _lenta->requestMarkRead(pi.id);
  post_widget->setTitle(pi.title);
  QString stylesheet;
  if(!_opts->po.stylesheetPath().isEmpty())
    stylesheet = "<link rel=\"stylesheet\" type=\"text/css\" href=\""+_opts->po.stylesheetPath()+"\"/>";
  QString xml = "<html>"+stylesheet+"<body>"+pi.content+"</body></html>";
  post_widget->populatePost(xml);
  if(!post_options_widget->backWidget())
    showOptions();
  else
    ui->stackedWidget->setCurrentWidget(post_options_widget->backWidget());
}

void MainWindow::returnSpecialOptions()
{
  switch(_opts->rt) {
  break;case ReaderType::Post: {
    ;
  }
  break;case ReaderType::Twinkle: {
    returnTwinkleOptions();
  }
  break; default:
    ;
  }

}

void MainWindow::returnGeneralOptions()
{
  if(!options_widget->backWidget())
    showSummary();
  else
    ui->stackedWidget->setCurrentWidget(options_widget->backWidget());
}

void MainWindow::connectionChange()
{
  ui->stackedWidget->setCurrentWidget(conn_widget);
}

void MainWindow::keyPressEvent(QKeyEvent * event)
{
  qDebug()<<"Main window";
  if(event->key() == Qt::Key_Comma) {
    event->accept();
    if(ui->stackedWidget->currentWidget()==feeds_widget) {
      QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context2, Qt::NoModifier);
      QCoreApplication::postEvent (feeds_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==posts_widget) {
      QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context2, Qt::NoModifier);
      QCoreApplication::postEvent (posts_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==summary_widget) {
      QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context2, Qt::NoModifier);
      QCoreApplication::postEvent (summary_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==twinkle_widget) {
      QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context2, Qt::NoModifier);
      QCoreApplication::postEvent (twinkle_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==conn_widget) {
      QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context2, Qt::NoModifier);
      QCoreApplication::postEvent (conn_widget, ev);
    }

    else {
      event->ignore();
//      qDebug()<<"ignored";
      return;
    }
//    qDebug()<<"key event posted";
    return;
  }
  else if(event->key() == Qt::Key_Period) {
    event->accept();
    if(ui->stackedWidget->currentWidget()==feeds_widget) {
        QKeyEvent *ev = new QKeyEvent ( QEvent::KeyPress, Qt::Key_Context1, Qt::NoModifier);
        QCoreApplication::postEvent (feeds_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==posts_widget) {
        QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context1, Qt::NoModifier);
        QCoreApplication::postEvent (posts_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==summary_widget) {
        QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context1, Qt::NoModifier);
        QCoreApplication::postEvent (summary_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==twinkle_widget) {
        QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context1, Qt::NoModifier);
        QCoreApplication::postEvent (twinkle_widget, ev);
    }
    else if(ui->stackedWidget->currentWidget()==conn_widget) {
        QKeyEvent *ev = new QKeyEvent(QEvent::KeyPress, Qt::Key_Context1, Qt::NoModifier);
        QCoreApplication::postEvent (conn_widget, ev);
    }
    else {
        event->ignore();
//        qDebug()<<"ignored";
        return;
    }
//    qDebug()<<"key event posted";
    return;
  }
  else {
    event->ignore();
    qDebug()<<event->key()<<"ignored";
  }
}

void MainWindow::closeEvent(QCloseEvent * event)
{
  if(_local->save(TwinkleUtils::DefaultDataPath(), TwinkleUtils::FeedsFile()))
    qDebug()<<"Cache saved:"<<TwinkleUtils::DefaultDataPath()+TwinkleUtils::FeedsFile();
  _opts->save(TwinkleUtils::DefaultDataPath(), TwinkleUtils::SessionFile());
  event->accept();
}

void MainWindow::handleAuthError()
{
  //TODO
}

void MainWindow::openSource()
{
  QDesktopServices::openUrl(_local->post(_posts_manager->currentPosts()[_cur_post]).link);
}

void MainWindow::hideProgress()
{
  emit status("");
}
