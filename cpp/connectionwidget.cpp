#include "connectionwidget.h"
#include "ui_connectionwidget.h"
#include "connectionmanager.h"
#include "common.hpp"
#include "mainoptions.h"
#include <QDebug>
#include <QTimer>
#include <QtWidgets/QApplication>

ConnectionWidget::ConnectionWidget(MainOptions *opts, QWidget *parent) :
  QWidget(parent), ui(new Ui::ConnectionWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  connect(ui->fore_button, SIGNAL(clicked()), this, SLOT(goFore()));
  connect(ui->back_button, SIGNAL(clicked()), this, SLOT(goBack()));
  if(opts) {
    ui->fore_button->setMinimumHeight(opts->button_height);
    ui->back_button->setMinimumHeight(opts->button_height);
  }
  setStatus("");
}

ConnectionWidget::~ConnectionWidget()
{
  delete ui;
}

void ConnectionWidget::populateList(QStringList items)
{
  ui->list->clear();
  ui->list->addItems(items);
  if(ui->list->count()>0)
    ui->list->setCurrentRow(0);
}

void ConnectionWidget::keyPressEvent(QKeyEvent * event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
  if(event->modifiers()&Qt::AltModifier) {
    if(event->key()>=Qt::Key_1 && event->key()<=Qt::Key_9) {
      ui->list->setCurrentRow(event->key()-Qt::Key_1);
      goFore();
      event->accept();
      return;
    }
    event->ignore();
  }
  else {
    event->ignore();
  }
}

void ConnectionWidget::setManager(ConnectionManager *manager)
{
  _manager = manager;
  connect(_manager, SIGNAL(listPopulated(QStringList)), this, SLOT(populateList(QStringList)));
  connect(ui->list, SIGNAL(activated(QModelIndex)), this, SLOT(goFore()));
}

void ConnectionWidget::goBack()
{
  _manager->goBack();
}

void ConnectionWidget::goFore()
{
  if(ui->list->count()>0)
    _manager->selectItem(ui->list->currentRow());
}

void ConnectionWidget::setStatus(QString text)
{
  ui->status->setText(text);
  ui->status->setVisible(!text.isEmpty());
}
