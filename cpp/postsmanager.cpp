#include "postsmanager.h"
#include "lentamanager.h"
#include "locallenta.h"
#include "lentatypes.h"


PostsManager::PostsManager(QObject *parent) :
                           QObject(parent) {}

void PostsManager::setLenta(LentaManager *lenta, LocalLenta *local)
{
  _local = local;
}

void PostsManager::populateList()
{
  FeedInfo fi = _local->feed(_feed_idx);
  PostsListModel* model = new PostsListModel(this);
  _posts_ids = _local->posts(fi.md5);
  QVector<PostInfo> ps;
  for(int i=0; i<_posts_ids.size(); ++i)
    ps.push_back(_local->post(_posts_ids[i]));
  model->setPosts(ps);
  model->setFont(_font);
  populateList(model, fi.title);
}

void PostsManager::populateList(PostsListModel *model, QString title)
{
  emit requestPopulate(model, title);
}

void PostsManager::setFont(const QFont &font)
{
  _font = font;
}

void PostsManager::setCurrentFeed(int feed_idx)
{
  _feed_idx = feed_idx;
  populateList();
}

int PostsManager::currentFeed() const
{
  return _feed_idx;
}

const QStringList &PostsManager::currentPosts() const
{
  return _posts_ids;
}

void PostsManager::postsDownloading(qint64 cur, qint64 tot)
{
  emit progress(tr("Downloading %1/%2").arg(cur).arg(tot));
}

void PostsManager::goBack()
{
  emit back();
}

void PostsManager::selectItem(int val)
{
  emit itemSelected(val);
}
