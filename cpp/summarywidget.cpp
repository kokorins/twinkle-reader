#include "summarywidget.h"
#include "ui_summarywidget.h"
#include "lentatypes.h"
#include "common.hpp"
#include "summarymanager.h"
#include <QKeyEvent>

SummaryWidget::SummaryWidget(QWidget *parent) : QWidget(parent), ui(new Ui::SummaryWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  setStatus("");
  ui->update_progress->setVisible(false);
}

SummaryWidget::~SummaryWidget()
{
  delete ui;
}

void SummaryWidget::setManager(SummaryManager *manager)
{
  _manager = manager;
  connect(_manager, SIGNAL(sigProperties(SummaryProperties)), this, SLOT(setProps(SummaryProperties)));
  connect(ui->fore_button, SIGNAL(clicked()), _manager, SIGNAL(fore()));
  connect(ui->back_button, SIGNAL(clicked()), _manager, SIGNAL(back()));
  connect(ui->options_button, SIGNAL(clicked()), _manager, SIGNAL(options()));
  connect(ui->update_button, SIGNAL(clicked()), _manager, SIGNAL(update()));
}

void SummaryWidget::setProps(const SummaryProperties &props)
{
  ui->unread_edit->setText(QString::number(props.unread_posts));
  ui->new_edit->setText(QString::number(props.new_posts));
}

void SummaryWidget::setStatus(const QString &text)
{
  ui->status->setText(text);
  ui->status->setVisible(!text.isEmpty());
}

void SummaryWidget::keyPressEvent(QKeyEvent *event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}
