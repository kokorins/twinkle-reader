#ifndef LENTATYPES_H
#define LENTATYPES_H
#include <QString>
#include <QDomElement>
#include <QIODevice>
#include <QDateTime>
#include <QVector>
#include <QAbstractTableModel>
#include <QFont>
#include <QVariant>
#include <QPixmap>

/**
 * \breif Summary information: number of new and number of unread
 */
struct SummaryProperties {
  SummaryProperties();
  int unread_posts;
  int new_posts;
  void save(QIODevice* io);
  void load(QIODevice* io);
};

/**
 * \breif information about feeds
 */
struct FeedInfo {
  enum EFeedField {
    MD5,
    Title,
    Link,
    Unread,
    FavUrl,
    Fav,
    EFeedFieldNum
  };

  ///Feeds identifier
  QString md5;
  ///Feeds title
  QString title;
  ///Feeds origin
  QString link;
  ///Number of unread
  int unread;
  QString fav_url;
  QPixmap fav;

  void save(QIODevice* io);
  void load(QIODevice* io);
};

class FeedsListModel : public QAbstractTableModel
{
  enum ERoles {
    MD5 = Qt::UserRole+1,
    TITLE,
    LINK,
    UNREAD,
    FAVICON,
    ERolesLen
  };

public:
  FeedsListModel(QObject *parent = 0);
  void setFeeds(const QVector<FeedInfo>&);
  void swapFeeds(QVector<FeedInfo>&);
  void setFont(const QFont&);
  virtual int rowCount(const QModelIndex& model=QModelIndex()) const;
  virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
private:
  QVector<FeedInfo> _feeds;
  QFont _font;
};

struct PostInfo {
  QString id;
  bool read;
  QString link;
  QString md5;
  QString title;
  QDateTime issued;
  QString content;
  void save(QIODevice* io);
  void load(QIODevice* io);
};

class PostsListModel : public QAbstractTableModel {
public:
  enum ERoles {
    ID = Qt::UserRole+1,
    READ,
    LINK,
    MD5,
    TITLE,
    ISSUED,
    CONTENT,
    SORT,
    ERolesLen
  };

public:
  PostsListModel(QObject *parent = 0);
  void setPosts(const QVector<PostInfo>&);
  void setFont(const QFont&);
  virtual int rowCount(const QModelIndex& model=QModelIndex()) const;
  virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;
  virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
private:
  QVector<PostInfo> _posts;
  QFont _font;
};


#endif // LENTATYPES_H
