#include "twinkleoptions.h"
#include "common.h"
#include <QObject>

TwinkleOptions::TwinkleOptions() :
    interval(300), font_size(32),
    splitters("[\\s,\\.]"), no_activity(TwinkleOptions::Default) {}

void TwinkleOptions::save(QIODevice *io)
{
  TwinkleUtils::WriteInt(io, kVersion);
  TwinkleUtils::WriteInt(io, interval);
  TwinkleUtils::WriteInt(io, font_size);
  TwinkleUtils::WriteFont(io, font);
  TwinkleUtils::WriteString(io, splitters);
  TwinkleUtils::WriteInt(io, no_activity);
}

void TwinkleOptions::load(QIODevice *io)
{
  int ver = TwinkleUtils::ReadInt(io);
  if(ver!=kVersion)
    return;
  interval = TwinkleUtils::ReadInt(io);
  font_size = TwinkleUtils::ReadInt(io);
  font = TwinkleUtils::ReadFont(io);
  splitters = TwinkleUtils::ReadString(io);
  no_activity = (ENoActivity)TwinkleUtils::ReadInt(io);
}

QString ToString(TwinkleOptions::ENoActivity type)
{
  switch(type) {
  break;case TwinkleOptions::Default:
    return QObject::tr("Default");
  break;case TwinkleOptions::Backlight:
    return QObject::tr("Always on");
  break; default:
    return "";
  }
}
