#include "optionswidget.h"
#include "common.hpp"
#include "ui_optionswidget.h"
#include "widgettypes.h"
#include <QKeyEvent>
#include <QThread>

OptionsWidget::OptionsWidget(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::OptionsWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
}

OptionsWidget::~OptionsWidget()
{
  delete ui;
}

void OptionsWidget::init(EReaderTypes tp)
{
  using namespace options;
  ui->post_type_combo->setCurrentIndex(tp);
  connect(ui->post_type_combo, SIGNAL(currentIndexChanged(int)), this, SLOT(changeType(int)));
  connect(ui->back_button, SIGNAL(clicked()), this, SIGNAL(back()));
  QState* post = new PostState(this);
  QState* twinkle = new TwinkleState(this);
  post->addTransition(this, SIGNAL(twinkle()), twinkle);
  twinkle->addTransition(this, SIGNAL(post()), post);
  _m.addState(post);
  _m.addState(twinkle);
  switch(tp) {
  break; case ReaderType::Post:
    _m.setInitialState(post);
  break; case ReaderType::Twinkle:
    _m.setInitialState(twinkle);
  break; default:
    _m.setInitialState(post);
  }
  _m.start();
}

void OptionsWidget::keyPressEvent(QKeyEvent * event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}

void OptionsWidget::setBackWidget(QWidget *back_widget)
{
  _back = back_widget;
}

QWidget *OptionsWidget::backWidget()
{
  return _back;
}

void OptionsWidget::changeType(int tp)
{
  switch((ReaderType::EReaderTypes)tp) {
  break; case ReaderType::Post:
    emit post();
  break; case ReaderType::Twinkle:
    emit twinkle();
  break; default:
    emit post();
  }
  emit typeChanged(tp);
}

namespace options {

void PostState::onEntry(QEvent *)
{
  connect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SIGNAL(fore()));
}

void PostState::onExit(QEvent *)
{
  disconnect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SIGNAL(fore()));
}

void TwinkleState::onEntry(QEvent *)
{
  connect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SIGNAL(fore()));
}

void TwinkleState::onExit(QEvent *)
{
  disconnect(_wid->ui->fore_button, SIGNAL(clicked()), _wid, SIGNAL(fore()));
}

} //namespace options
