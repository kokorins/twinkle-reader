#ifndef SUMMARYMANAGER_H
#define SUMMARYMANAGER_H

#include <QObject>
#include "lentatypes.h"

class SummaryManager : public QObject {
  Q_OBJECT
public:
  explicit SummaryManager(QObject *parent = 0);
  void setProps(const SummaryProperties& props);
  void goFore();
  void goBack();
  void goOptions();
signals:
  void sigProperties(SummaryProperties);
  void back();
  void fore();
  void options();
  void update();
};

#endif // SUMMARYMANAGER_H
