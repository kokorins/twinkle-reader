#ifndef CONNECTIONMANAGER_H
#define CONNECTIONMANAGER_H

#include <QObject>
#include <QStringList>
#include <QList>

class ConnectionManager : public QObject {
  Q_OBJECT
public:
  explicit ConnectionManager(QObject *parent = 0);
  void populateList(QStringList items);
public slots:
  void selectItem(int);
  void goBack();
signals:
  void listPopulated(QStringList items);
  void itemSelected(int);
  void back();
};

#endif // CONNECTIONMANAGER_H
