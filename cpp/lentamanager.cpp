#include "lentamanager.h"
#include "oauthmanager.h"
#include "locallenta.h"
#include <QDomDocument>
#include <QDebug>
#include <QXmlSimpleReader>
#include <QThread>
#include <QPixmap>

const QString LentaManager::kLentaUrl ="https://api-lenta.yandex.ru";

LentaManager::LentaManager(const QString& token, QObject *parent) : QObject(parent), _token(token) {}

void LentaManager::requestMarkRead(QString id)
{
  QNetworkAccessManager *manager = new QNetworkAccessManager(this);
  QNetworkRequest nr(QUrl(kLentaUrl+"/posts/"+id+"/meta"));
  nr.setRawHeader("Content-type", "application/x-yandex-lenta+xml; type=post; charset=utf-8");
  nr.setRawHeader("Authorization", (QString("OAuth ")+_token).toLocal8Bit());
  QString str="<post><id>"+id+"</id><read/></post>";
  PostInfo pi = _local->post(id);
  pi.read = true;
  _local->setPost(id, pi);
  QByteArray ba = str.toLocal8Bit();
  _replies[P] = manager->put(nr,ba);
  connect(_replies[P], SIGNAL(finished()), this, SLOT(updateRead()));
  connect(_replies[P], SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
}

void LentaManager::cancel(LentaManager::EReplyType rep_type)
{
  QMap<EReplyType, QNetworkReply*>::iterator it = _replies.find(rep_type);
  if(it==_replies.end())
    return;
  if(*it && (*it)->isRunning())
    (*it)->abort();
}

void LentaManager::cancelAll()
{
  QMap<EReplyType, QNetworkReply*>::iterator it = _replies.begin();
  for(; it!=_replies.end(); ++it) {
    if(*it&& (*it)->isRunning())
      (*it)->abort();
  }
}

void LentaManager::requestSummary()
{
  QNetworkAccessManager *manager = new QNetworkAccessManager(this);
  QNetworkRequest nr(QUrl(kLentaUrl+"/stat/unread"));
  nr.setRawHeader("Content-type", "application/x-yandex-lenta+xml; type=stat; charset=utf-8");
  nr.setRawHeader("Authorization", (QString("OAuth ")+_token).toLocal8Bit());
  _replies[SU] = manager->get(nr);
  connect(_replies[SU], SIGNAL(finished()), this, SLOT(summaryUnreadFinished()));
  connect(_replies[SU], SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
  connect(_replies[SU], SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(summaryDownloadProgress(qint64,qint64)));
  nr.setUrl(kLentaUrl+"/stat/new");
  _replies[SN] = manager->get(nr);
  connect(_replies[SN], SIGNAL(finished()), this, SLOT(summaryNewFinished()));
  connect(_replies[SN], SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
  connect(_replies[SN], SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(summaryDownloadProgress(qint64,qint64)));
}

void LentaManager::summaryUnreadFinished()
{
  QDomDocument doc;
  if(!doc.setContent(_replies[SU]->readAll(), false)) {
      qDebug()<<"Incorrect xml";
      return;
  }
  QDomElement root = doc.documentElement();
  QDomElement elem = root.firstChildElement("unread");
  int val = 0;
  if(!elem.text().isEmpty())
      val = elem.text().toInt();
  SummaryProperties sp = _local->summary();
  sp.unread_posts = val;
  _local->setSummary(sp);
  _replies[SU]->deleteLater();
}

void LentaManager::summaryNewFinished()
{
  QDomDocument doc;
  if(!doc.setContent(_replies[SN]->readAll(), false)) {
      qDebug()<<"Incorrect xml";
      return;
  }
  QDomElement root = doc.documentElement();
  QDomElement elem = root.firstChildElement("new");
  int val = 0;
  if(!elem.text().isEmpty())
      val = elem.text().toInt();
  SummaryProperties sp = _local->summary();
  sp.new_posts = val;
  _local->setSummary(sp);
  _replies[SN]->deleteLater();
}

void LentaManager::error(QNetworkReply::NetworkError err)
{
  qDebug()<<"NetError:"<<err;
  if(err != QNetworkReply::OperationCanceledError)
    emit connectionError();
}

void LentaManager::summaryDownloadProgress(qint64 cur, qint64 tot)
{
  emit summaryDownloading(cur, tot);
}

void LentaManager::requestFeeds()
{
  QNetworkAccessManager *manager = new QNetworkAccessManager(this);
  QNetworkRequest nr(QUrl(kLentaUrl+"/subscriptions"));
  nr.setRawHeader("Content-type", "application/x-yandex-lenta+xml; type=subscriptions; charset=utf-8");
  nr.setRawHeader("Authorization", (QString("OAuth ")+_token).toLocal8Bit());
  _replies[F] = manager->get(nr);
  connect(_replies[F], SIGNAL(finished()), this, SLOT(feedsFinished()));
  connect(_replies[F], SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
  connect(_replies[F], SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(feedsDownloadProgress(qint64,qint64)));
}

void LentaManager::feedsFinished()
{
  QDomDocument doc;
  if(!doc.setContent(_replies[F]->readAll(), false)) {
    qDebug()<<"Incorrect xml";
    return;
  }
  QDomElement root = doc.documentElement();
  QDomElement feed = root.firstChildElement("feed");
  QVector<FeedInfo> feeds;
  while(!feed.isNull()) {
    FeedInfo fi;
    fi.unread = feed.attribute("unread_count").toInt();
    qDebug()<<fi.unread;
    fi.md5 = feed.firstChildElement("md5").text();
    fi.link = feed.firstChildElement("link").attribute("href");
    fi.title = feed.firstChildElement("title").text();
    fi.unread = feed.attribute("unread_count").toInt();
    fi.fav_url = feed.firstChildElement("favicon").attribute("href");
    feeds.push_back(fi);
    requestFavicon(fi.md5);
    feed = feed.nextSiblingElement("feed");
  }
  _local->setFeeds(feeds);
}

void LentaManager::feedsDownloadProgress(qint64 cur, qint64 tot)
{
  emit feedsDownloading(cur, tot);
}

void LentaManager::requestPosts(QString feed_md5)
{

  QNetworkAccessManager *manager = new QNetworkAccessManager(this);
  QNetworkRequest nr(QUrl(kLentaUrl+"/posts?md5="+feed_md5));
  nr.setRawHeader("Content-type", "application/x-yandex-lenta+xml; type=posts; charset=utf-8");
  nr.setRawHeader("Authorization", (QString("OAuth ")+_token).toLocal8Bit());
  _replies[Ps] = manager->get(nr);
  connect(_replies[Ps], SIGNAL(finished()), this, SLOT(postsFinished()));
  connect(_replies[Ps], SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
  connect(_replies[Ps], SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(postsDownloadProgress(qint64,qint64)));
}

void LentaManager::postsFinished()
{
  QDomDocument doc;
  if(!doc.setContent(_replies[Ps]->readAll(), false)) {
    qDebug()<<"Incorrect xml";
    return;
  }
  QDomElement root = doc.documentElement();
  QDomElement post = root.firstChildElement("post");
  QString feed_md5;
  QString feed;
  PostInfo pi;
  while(!post.isNull()) {
    pi.read = false;
    pi.id = post.attribute("id");
    QDomElement post_elem = post.firstChildElement();
    while(!post_elem.isNull()) {
      if(post_elem.tagName()=="feed") {
        feed = post_elem.attribute("href");
        feed_md5 = feed.split(QRegExp("/")).back();
        pi.md5 = feed_md5;
      }
      else if(post_elem.tagName() == "entry") {
        QDomElement entry = post_elem.firstChildElement();
        while(!entry.isNull()) {
          if(entry.tagName()=="link")
            pi.link = entry.attribute("href");
          else if(entry.tagName()=="title")
            pi.title = entry.text();
          else if(entry.tagName()=="issued") {
            qDebug()<<entry.text();
            pi.issued = QDateTime::fromString(entry.text(),"yyyy-MM-ddTHH:mm:ssZ");
            qDebug()<<pi.issued;
          }
          else if(entry.tagName()=="content") {
            QString xml_str;
            QTextStream ts(&xml_str);
            entry.save(ts, 1);
            pi.content = xml_str;
          }
          entry = entry.nextSiblingElement();
        }
      }
      else if(post_elem.tagName() == "read") {
        pi.read = true;
      }
      post_elem = post_elem.nextSiblingElement();
    }
    _local->setPost(pi.id, pi);
    post = post.nextSiblingElement("post");
  }
}

void LentaManager::postsDownloadProgress(qint64 cur, qint64 tot)
{
  emit postsDownloading(cur, tot);
}

void LentaManager::updateRead()
{
  _replies[P]->deleteLater();
}

void LentaManager::setLocal(LocalLenta *local)
{
  _local = local;
}

void LentaManager::requestFavicon(QString feed_md5)
{
  int idx = _local->searchFeed(FeedInfo::MD5, feed_md5);
  if(idx<0)
    return;
  FeedInfo fi = _local->feed(idx);
  QNetworkRequest nr(QUrl(fi.fav_url));
  QNetworkAccessManager *manager = new QNetworkAccessManager(this);
  connect(manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(faviconFinished(QNetworkReply*)));
  manager->get(nr);
}

void LentaManager::faviconFinished(QNetworkReply* rep)
{
  if(rep->error() != QNetworkReply::NoError)
  return;
  int idx = _local->searchFeed(FeedInfo::FavUrl, rep->request().url().toString());
  if(idx<0)
    return;
  QPixmap qp;
  if(qp.loadFromData(rep->readAll())) {
    FeedInfo fi = _local->feed(idx);
    fi.fav = qp;
    _local->updateFeed(idx, fi);
  }
  rep->deleteLater();
}
