#include "widgettypes.h"
#include <QObject>

QString ConnectionType::ToString(EProviderType type)
{
  switch(type) {
  break; case NotConnected:
    return QObject::tr("Not connected");
  break; case Yandex:
    return QObject::tr("Yandex");
  break; default:
    return "";
  }
}

QString ConnectionType::ToString(EConnectionType type)
{
  switch(type) {
    break; case Summary:
      return QObject::tr("Summary");
    break; case Internal:
      return QObject::tr("Internal auth");
    break; default:
      return "";
  }
}

int ConnectionType::ToKey(EConnectionType type)
{
  switch(type) {
    break; case Summary:
      return Qt::Key_S;
    break; case Internal:
      return Qt::Key_I;
    break; default:
      return -1;
  }
}

QString ReaderType::ToString(EReaderTypes type) {
  switch(type) {
  break; case Post:
    return QObject::tr("Post");
  break; case Twinkle:
    return QObject::tr("Twinkle");
  break; default:
    return "";
  }
}
