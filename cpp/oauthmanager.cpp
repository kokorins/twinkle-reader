#include "oauthmanager.h"
#include "common.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSslError>
#include <QList>
#include <QDebug>
#include <QDir>
#include <QDesktopServices>
#include <QStringList>
#include <QFile>
#include <QSslError>
#include <QSslConfiguration>
#include <QString>
#include <functional>
#include <algorithm>
#include <functional>

OAuthManager::OAuthManager(QObject *parent) :
  QObject(parent), _reply(0), _req_type(Code), _has_token(false), _has_code(false) {}

bool OAuthManager::requestCode()
{
  if(!_has_id)
    return false;
  QDesktopServices::openUrl(codeUrl());
  return true;
}

bool OAuthManager::requestToken()
{
  QNetworkAccessManager *manager = new QNetworkAccessManager(this);
  switch(_req_type) {
  break; case Code: {
    if(!_has_id || !_has_code) {
      qDebug()<<"Application has no id";
      return false;
    }
    QNetworkRequest nr(QUrl("https://oauth.yandex.ru/token"));
    QString header("grant_type=authorization_code&code="+_code+"&client_id="+_id+"&client_secret="+_pass);
    QByteArray ba(header.toLocal8Bit());
    nr.setRawHeader("Content-type", "application/x-www-form-urlencoded");
    nr.setRawHeader("Content-Length", QString::number(ba.size()).toLocal8Bit());
    _reply = manager->post(nr, ba);
    connect(_reply, SIGNAL(finished()), this, SLOT(acquireToken()));
    connect(_reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));
    connect(_reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(acquireTokenError(QNetworkReply::NetworkError)));
    connect(_reply, SIGNAL(downloadProgress(qint64,qint64)),
            this, SLOT(acqureTokenProgress(qint64,qint64)));
  }
  break; default:
    qDebug()<<"Incorrect token request type";
    return false;
  }
  return true;
}

bool OAuthManager::hasToken() const
{
  return _has_token;
}

bool OAuthManager::isExpired() const
{
  if(!_expire.isValid())
    return true;
  else 
    return _expire<QDateTime::currentDateTime();
}

void OAuthManager::setCode(QString code)
{
  _code = code;
  _has_code = true;
}

void OAuthManager::setIdPass(QString id, QString pass)
{
  _id = id;
  _pass = pass;
  _has_id = true;
}

bool OAuthManager::restore(QString file_name)
{
  QString path = TwinkleUtils::DefaultDataPath();
  QFile q(path+file_name);
  if(!q.open(QFile::ReadOnly))
      return false;
  QStringList all = QString(q.readAll()).split(" ");
  _token = all[0];
  QString expire_str = all[1];
  _expire = QDateTime::fromString(expire_str);
  emit status(tr("token restored from ")+path+file_name);
  q.close();
  return true;
}

void OAuthManager::dump(QString file_name)
{
  QString path = TwinkleUtils::DefaultDataPath();
  QDir dir(path);
  if(!dir.exists())
    dir.mkpath(path);
  QFile q(path +  file_name);
  if (q.open(QFile::WriteOnly | QFile::Truncate)) {
    QTextStream out(&q);
    out <<_token<<" "<<_expire.toString();
    emit status(tr("token saved to ")+path+file_name);
    q.close();
  }
}

QUrl OAuthManager::codeUrl() const
{
  if(!_has_id)
    return QUrl("");
  return QUrl("https://m.oauth.yandex.ru/authorize?redirect_uri=https://oauth.yandex.ru/verification_code&response_type=code&display=popup&client_id="+_id);
}

QUrl OAuthManager::tokenUrl() const
{
  if(!_has_id)
    return QUrl("");
  return QUrl("https://m.oauth.yandex.ru/authorize?response_type=token&display=popup&client_id="+_id);
}

void OAuthManager::setRequestType(OAuthManager::ERequestType tp)
{
  _req_type = tp;
}

void OAuthManager::cancel()
{
  if(_reply && _reply->isRunning())
    _reply->abort();
}

void OAuthManager::acquireToken()
{
  if(!_reply || !_reply->isFinished()) {
    _token = "";
    _has_token = false;
    return;
  }
  QMap<QString,QString> values;
  QString ans(_reply->readAll());
  qDebug()<<ans;
  if(ParseTokenJson(ans, values)) {
    _token = values["access_token"];
    if(values.find("expires_in")!=values.end()) {
      _expire = QDateTime::currentDateTime();
      int secs = values["expires_in"].toInt();
      _expire = _expire.addSecs(secs);
    }
    _has_token = true;
    qDebug()<<_token<<_expire;
  }
  else {
    _token = "";
    _has_token = false;
  }
  emit replyFinished();
  _reply->deleteLater();
}

QString OAuthManager::token() const
{
  return _token;
}

QDateTime OAuthManager::expiration() const
{
  return _expire;
}

QString OAuthManager::code() const
{
  return _code;
}

void OAuthManager::setToken(QString token, QDateTime expire)
{
  _token = token;
  _expire = expire;
  _has_token = true;
}

void OAuthManager::acquireTokenError(QNetworkReply::NetworkError err)
{
  emit status(tr("Network error:")+QString(err));
  emit replyError();
}

void OAuthManager::acqureTokenProgress(qint64 cur, qint64 tot)
{
  emit replyProgress(cur, tot);
}

bool OAuthManager::ParseTokenJson(QString token_str, QMap<QString, QString> &values)
{
  QStringList toks = token_str.split(QRegExp("[\\{\\},]"));
  toks.erase(std::remove_if(toks.begin(), toks.end(), std::mem_fun_ref(&QString::isEmpty)), toks.end());
  QRegExp rx("(\\w+)");
  for(int i=0; i<toks.size(); ++i) {
    QStringList elems;
    int pos = 0;
    while ((pos = rx.indexIn(toks[i], pos)) != -1) {
      elems << rx.cap(1);
      pos += rx.matchedLength();
    }
    if(elems.size()==2) {
      values[elems[0]]=elems[1];
    }
  }
  return values.find("access_token")!=values.end();
}

void OAuthManager::sslErrors(QList<QSslError> ss_errs)
{
  QString errors;
  foreach(const QSslError& err, ss_errs)
    errors.append(err.errorString());
  emit status(errors);
  emit replyError();
}
