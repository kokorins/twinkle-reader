#include "postwidget.h"
#include "common.hpp"
#include "ui_postwidget.h"
#include <QKeyEvent>
#include <QDesktopServices>
#include <QDebug>

PostWidget::PostWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::PostWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  connect(ui->back_button, SIGNAL(clicked()), this, SLOT(goBack()));
  connect(ui->fore_button, SIGNAL(clicked()), this, SLOT(goFore()));
  ui->web->page()->setLinkDelegationPolicy(QWebPage::DelegateAllLinks);
  connect(ui->web, SIGNAL(linkClicked(QUrl)), this, SLOT(openExternal(QUrl)));
  connect(ui->web, SIGNAL(urlChanged(QUrl)), this, SLOT(openExternal(QUrl)));
}

PostWidget::~PostWidget()
{
  delete ui;
}

void PostWidget::populatePost(QString text)
{
  ui->web->setHtml(text);
}

void PostWidget::setTitle(QString title)
{
  ui->name_label->setText(title);
}

void PostWidget::goBack()
{
  emit back();
}

void PostWidget::goFore()
{
  emit fore();
}

void PostWidget::openExternal(QUrl url)
{
  if(url==QUrl("about:blank"))
    return;
  qDebug()<<url;
  QDesktopServices::openUrl(url);
}

void PostWidget::openSource()
{
  qDebug()<<"clicked";
  emit requestSource();
}

void PostWidget::keyPressEvent(QKeyEvent *event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}

void PostWidget::mousePressEvent(QMouseEvent * event)
{
  if(ui->name_label == this->childAt(event->pos())) {
    event->accept();
    openSource();
  }
  else {
    event->ignore();
  }
}

void PostWidget::setOpts(const PostOptions &po)
{
  _po = po;
}
