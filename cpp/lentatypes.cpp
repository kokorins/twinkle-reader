#include "lentatypes.h"
#include "common.h"
#include <QDebug>
#include <QSize>
#include <QFontMetrics>
#include <QVariant>
#include <QModelIndex>
#include <QIcon>
#include <QFont>
#include <QPixmap>

SummaryProperties::SummaryProperties() :
    unread_posts(0), new_posts(0) { }

void SummaryProperties::save(QIODevice* io)
{
  TwinkleUtils::WriteInt(io, unread_posts);
  TwinkleUtils::WriteInt(io, new_posts);
}

void SummaryProperties::load(QIODevice *io)
{
  unread_posts = TwinkleUtils::ReadInt(io);
  new_posts = TwinkleUtils::ReadInt(io);
}

void FeedInfo::save(QIODevice *io)
{
  TwinkleUtils::WriteString(io, md5);
  TwinkleUtils::WriteString(io, title);
  TwinkleUtils::WriteString(io, link);
  TwinkleUtils::WriteInt(io, unread);
  TwinkleUtils::WriteString(io, fav_url);
  TwinkleUtils::WritePixmap(io, fav);
}

void FeedInfo::load(QIODevice *io)
{
  md5 = TwinkleUtils::ReadString(io);
  title = TwinkleUtils::ReadString(io);
  link = TwinkleUtils::ReadString(io);
  unread = TwinkleUtils::ReadInt(io);
  fav_url = TwinkleUtils::ReadString(io);
  fav = TwinkleUtils::ReadPixmap(io);
}

void PostInfo::save(QIODevice *io)
{
  TwinkleUtils::WriteString(io, id);
  TwinkleUtils::WriteInt(io, read);
  TwinkleUtils::WriteString(io, link);
  TwinkleUtils::WriteString(io, md5);
  TwinkleUtils::WriteInt(io, issued.toMSecsSinceEpoch());
  TwinkleUtils::WriteString(io, title);
  TwinkleUtils::WriteString(io, content);
}

void PostInfo::load(QIODevice *io)
{
  id = TwinkleUtils::ReadString(io);
  read = TwinkleUtils::ReadInt(io);
  link = TwinkleUtils::ReadString(io);
  md5 = TwinkleUtils::ReadString(io);
  qint64 iss = TwinkleUtils::ReadInt(io);
  issued = QDateTime::fromMSecsSinceEpoch(iss);
  title = TwinkleUtils::ReadString(io);
  content = TwinkleUtils::ReadString(io);
}

FeedsListModel::FeedsListModel(QObject *parent) :
                QAbstractTableModel(parent) {}

void FeedsListModel::setFeeds(const QVector<FeedInfo> &feeds)
{
  _feeds = feeds;
}

void FeedsListModel::swapFeeds(QVector<FeedInfo> & feeds)
{
  qSwap(_feeds, feeds);
}

void FeedsListModel::setFont(const QFont &font)
{
  _font = font;
}

int FeedsListModel::rowCount(const QModelIndex &) const
{
  return _feeds.size();
}

int FeedsListModel::columnCount(const QModelIndex &) const
{
  return 3;
}

QVariant FeedsListModel::data(const QModelIndex &index, int role) const
{
//  qDebug()<<index.row()<<role;
  int i = index.row();
  int j = TITLE;
  if(role == Qt::DisplayRole) {
    switch(j) {
    break; case MD5: {
      return _feeds[i].md5;
    }
    break; case TITLE: {
      QString res = _feeds[i].title;
      if(_feeds[i].unread)
        res += tr(" (%1)").arg(_feeds[i].unread);
      return res;
    }
    break; case LINK: {
      return _feeds[i].link;
    }
    break; case UNREAD: {
      return _feeds[i].unread;
    }
    break; case FAVICON: {
      return QVariant();
    }
    break; default:
      return "";
    }
  }
  if(role == Qt::DecorationRole) {
    if(j==TITLE) {
//      qDebug()<<_feeds[i].fav.size();
//      if(_feeds[i].fav.size().width()<1)
//        return QPixmap(16,16);
      return _feeds[i].fav;
    }
  }
  if(role == Qt::FontRole) {
    if(_feeds[i].unread>0) {
       QFont font = _font;
       font.setBold(true);
       return font;
    }
    return _font;
  }
  return QVariant();
}

PostsListModel::PostsListModel(QObject *parent) : QAbstractTableModel(parent) {}

void PostsListModel::setPosts(const QVector<PostInfo> &posts)
{
  _posts = posts;
}

void PostsListModel::setFont(const QFont &font)
{
  _font = font;
}

int PostsListModel::rowCount(const QModelIndex &) const
{
  return _posts.size();
}

int PostsListModel::columnCount(const QModelIndex &parent) const
{
  return 7;
}

QVariant PostsListModel::data(const QModelIndex &index, int role) const
{
  int i = index.row();
  int j = index.column();

  if(role == Qt::DisplayRole) {
    switch(j) {
    break; case ID: {
      return _posts[i].id;
    }
    break; case READ: {
      return QString::number(_posts[i].read);
    }
    break; case LINK: {
      return _posts[i].link;
    }
    break; case MD5: {
      return _posts[i].md5;
    }
    break; case TITLE: {
      return _posts[i].title;
    }
    break; case ISSUED: {
      return _posts[i].issued.toString();
    }
    break; case CONTENT: {
      return _posts[i].content;
    }
    break; default:
      return "";
    }
  }
  if(role == SORT) {
    switch(j) {
    break; case ID: {
      return _posts[i].id;
    }
    break; case READ: {
      return _posts[i].read;
    }
    break; case LINK: {
      return _posts[i].link;
    }
    break; case MD5: {
      return _posts[i].md5;
    }
    break; case TITLE: {
      return _posts[i].title;
    }
    break; case ISSUED: {
      qDebug()<<_posts[i].issued;
      return _posts[i].issued;
    }
    break; case CONTENT: {
      return _posts[i].content;
    }
    break; default:
      return "";
    }
  }
  if(role == Qt::FontRole) {
    if(!_posts[i].read) {
       QFont font = _font;
       font.setBold(true);
       return font;
    }
    return _font;
  }
  return QVariant();
}
