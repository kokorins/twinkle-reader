QT += core gui xml network widgets webkitwidgets declarative quick

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    summarywidget.cpp \
    twinklewidget.cpp \
    postwidget.cpp \
    twinkleoptionswidget.cpp \
    twinkleoptions.cpp \
    oauthmanager.cpp \
    lentatypes.cpp \
    lentamanager.cpp \
    connectionwidget.cpp \
    feedswidget.cpp \
    postswidget.cpp \
    noactivityeventfilter.cpp \
    optionswidget.cpp \
    common.cpp \
    widgettypes.cpp \
    yacodewidget.cpp \
    postoptions.cpp \
    locallenta.cpp \
    connectionmanager.cpp \
    roleitemmodel.cpp \
    feedsmanager.cpp \
    mainoptions.cpp \
    postsmanager.cpp \
    postoptionswidget.cpp \
    summarymanager.cpp \
    feedsmodel.cpp

HEADERS += \
    mainwindow.h \
    summarywidget.h \
    twinklewidget.h \
    postwidget.h \
    twinkleoptionswidget.h \
    twinkleoptions.h \
    oauthmanager.h \
    lentatypes.h \
    lentamanager.h \
    common.hpp \
    common.h \
    connectionwidget.h \
    feedswidget.h \
    postswidget.h \
    noactivityeventfilter.h \
    optionswidget.h \
    widgettypes.h \
    yacodewidget.h \
    postoptions.h \
    locallenta.h \
    connectionmanager.h \
    roleitemmodel.h \
    feedsmanager.h \
    mainoptions.h \
    postsmanager.h \
    postoptionswidget.h \
    summarymanager.h \
    feedsmodel.h

FORMS += \
    mainwindow.ui \
    summarywidget.ui \
    twinklewidget.ui \
    postwidget.ui \
    twinkleoptionswidget.ui \
    connectionwidget.ui \
    feedswidget.ui \
    postswidget.ui \
    optionswidget.ui \
    yacodewidget.ui \
    postoptionswidget.ui

OTHER_FILES += \
    icons/script-import.png \
    icons/property.png \
    icons/lighthouse-shine.png \
    icons/feed--arrow.png \
    icons/door-open-out.png \
    icons/cross.png \
    icons/control-stop-square.png \
    icons/control-stop.png \
    icons/control-pause.png \
    icons/control.png \
    icons/arrow-circle-double.png \
    Feeds.qml \
    Posts.qml \
    Main.qml \
    Options.qml \
    AddFeed.qml \
    Post.qml

RESOURCES += \
    resources.qrc
