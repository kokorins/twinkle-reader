#include "postswidget.h"
#include "common.hpp"
#include "postsmanager.h"
#include "lentatypes.h"
#include "ui_postswidget.h"
#include <QSortFilterProxyModel>

PostsWidget::PostsWidget(QWidget *parent) :
    QWidget(parent), ui(new Ui::PostsWidget), sorting_proxy(new QSortFilterProxyModel())
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  ui->fore_button->setEnabled(false);
  connect(ui->back_button, SIGNAL(clicked()), this, SLOT(goBack()));
  connect(ui->fore_button, SIGNAL(clicked()), this, SLOT(goFore()));
  connect(ui->list, SIGNAL(activated(QModelIndex)), this, SLOT(goFore()));
  connect(ui->sort_alp_button, SIGNAL(clicked()), this, SLOT(slotSortAlph()));
  connect(ui->sort_date_button, SIGNAL(clicked()), this, SLOT(slotSortDate()));
  setStatus("");
  sort_prop.act_column = 1;
  sort_prop.orders <<Qt::DescendingOrder<<Qt::AscendingOrder;
}

void PostsWidget::goBack()
{
  _manager->goBack();
}

void PostsWidget::goFore()
{
  QAbstractItemModel* m = ui->list->model();
  if(m && m->rowCount()>0)
    _manager->selectItem(sorting_proxy->mapToSource(ui->list->currentIndex()).row());
}

void PostsWidget::slotSortAlph()
{
  sort_prop.changeActivate(0);
  sort();
}

void PostsWidget::slotSortDate()
{
  sort_prop.changeActivate(1);
  sort();
}

PostsWidget::~PostsWidget()
{
  delete ui;
}

void PostsWidget::populateList(PostsListModel* model, QString title)
{
  sorting_proxy->setSourceModel(model);
  ui->list->setModel(sorting_proxy);
  sort();
  ui->list->setWordWrap(true);
  ui->title->setText(title);
  if(model->rowCount()<1)
    emit emptyList();
  else
    emit nonEmptyList();
}

void PostsWidget::setStatus(QString text)
{
  ui->status->setText(text);
  ui->status->setVisible(!text.isEmpty());
}

void PostsWidget::sort()
{
  sorting_proxy->setSortRole(PostsListModel::SORT);
  sorting_proxy->sort(sort_prop.act_column, sort_prop.orders[sort_prop.act_column]);
  switch(sort_prop.act_column) {
  break; case 0: {
    if(sort_prop.orders[sort_prop.act_column] == Qt::AscendingOrder) {
      QIcon ic = QIcon::fromTheme("sort-alphabet-descending", QIcon(":icons/sort-alphabet-descending.png"));
      ui->sort_alp_button->setIcon(ic);
    }
    if(sort_prop.orders[sort_prop.act_column] == Qt::DescendingOrder) {
      QIcon ic = QIcon::fromTheme("sort-alphabet", QIcon(":icons/sort-alphabet.png"));
      ui->sort_alp_button->setIcon(ic);
    }
  }
  break; case 1: {
    if(sort_prop.orders[sort_prop.act_column] == Qt::AscendingOrder) {
      QIcon ic = QIcon::fromTheme("sort-date-descending", QIcon(":icons/sort-date-descending.png"));
      ui->sort_date_button->setIcon(ic);
    }
    if(sort_prop.orders[sort_prop.act_column] == Qt::DescendingOrder) {
      QIcon ic = QIcon::fromTheme("sort-date", QIcon(":icons/sort-date.png"));
      ui->sort_date_button->setIcon(ic);
    }
  }
  break; default:
    ;
  }
}

void PostsWidget::keyPressEvent(QKeyEvent * event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}

void PostsWidget::setVisible(bool visible)
{
  QWidget::setVisible(visible);
  QAbstractItemModel* m = ui->list->model();
  if(m && m->rowCount()>0) {
    if(ui->list->isVisible())
      ui->list->setFocus();
  }
}

void PostsWidget::setManager(PostsManager *manager)
{
  _manager = manager;
  _manager->setFont(font());
  connect(_manager, SIGNAL(requestPopulate(PostsListModel*, QString)), this, SLOT(populateList(PostsListModel*, QString)));

  using namespace posts;
  QState* idle = new IdleState(this);
  QState* empty = new EmptyState(this);
  QState* updating = new UpdatingState(this);
  idle->addTransition(this, SIGNAL(emptyList()), empty);
  empty->addTransition(this, SIGNAL(nonEmptyList()), idle);
  updating->addTransition(_manager, SIGNAL(postsUpdated()), idle);
  _m.addState(idle);
  _m.addState(empty);
  _m.addState(updating);
  _m.setInitialState(idle);
  _m.start();
}

namespace posts {
void IdleState::onEntry(QEvent *)
{
  qDebug()<<"posts:idle";
  QAbstractItemModel * model = _wid->ui->list->model();
  if(!model || model->rowCount()<1) {
    emit _wid->emptyList();
    return;
  }
  if(_wid->ui->list->isVisible())
    _wid->ui->list->setFocus();
  _wid->ui->fore_button->setEnabled(true);
}

void IdleState::onExit(QEvent *)
{
  _wid->ui->fore_button->setEnabled(false);
}

void EmptyState::onEntry(QEvent *)
{
  qDebug()<<"posts:empty";
}

void EmptyState::onExit(QEvent *)
{
}

void UpdatingState::onEntry(QEvent *)
{
  qDebug()<<"posts:updating";
  connect(_wid->_manager, SIGNAL(progress(QString)), _wid, SIGNAL(status(QString)));
}

void UpdatingState::onExit(QEvent *)
{
  disconnect(_wid->_manager, SIGNAL(progress(QString)), _wid, SIGNAL(status(QString)));
}

void SortingProp::changeActivate(int idx)
{
  act_column = idx;
  if(orders[act_column]==Qt::AscendingOrder)
    orders[act_column] = Qt::DescendingOrder;
  else if(orders[act_column] == Qt::DescendingOrder)
    orders[act_column] = Qt::AscendingOrder;
}

} //namespace posts


