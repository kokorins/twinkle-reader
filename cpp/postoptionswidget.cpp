#include "postoptionswidget.h"
#include "ui_postoptionswidget.h"
#include "common.hpp"
#include <QtWidgets/QFileDialog>

PostOptionsWidget::PostOptionsWidget(QWidget *parent) :
  QWidget(parent), _back(0),
  ui(new Ui::PostOptionsWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  connect(ui->back_button, SIGNAL(clicked()), this, SIGNAL(back()));
  connect(ui->stylesheet_button, SIGNAL(clicked()), this, SLOT(chooseFile()));
}

PostOptionsWidget::~PostOptionsWidget()
{
  delete ui;
}

void PostOptionsWidget::setBackWidget(QWidget *back)
{
  _back =  back;
}

QWidget *PostOptionsWidget::backWidget()
{
  return _back;
}

PostOptions PostOptionsWidget::opts() const
{
  PostOptions po;
  po.setStylesheetPath(ui->file_line->text());
  return po;
}

void PostOptionsWidget::keyPressEvent(QKeyEvent *event)
{
  if(event->key()==Qt::Key_Context2) {
    qDebug()<<"context2 emulated";
    ui->back_button->click();
    event->accept();
  }
  else {
    event->ignore();
  }
}

void PostOptionsWidget::chooseFile()
{
  QString file = QFileDialog::getOpenFileName(this, tr("Open stylesheet file"), ".", tr("css files (*.css)"));
  ui->file_line->setText(file);
}

void PostOptionsWidget::setOpts(const PostOptions &opts)
{
  ui->file_line->setText(opts.stylesheetPath());
}
