#ifndef CONNECTIONWIDGET_H
#define CONNECTIONWIDGET_H

#include "connectionmanager.h"
#include <QtWidgets/QWidget>

namespace Ui {
class ConnectionWidget;
}

class MainOptions;

/**
 * First widget in a row. Contains all the types of external and internal access.
 */
class ConnectionWidget : public QWidget {
  Q_OBJECT
public:
  explicit ConnectionWidget(MainOptions* opts,QWidget *parent = 0);
  virtual ~ConnectionWidget();
public:
  virtual void keyPressEvent(QKeyEvent *);
  void setManager(ConnectionManager * manager);
public slots:
  void goBack();
  void goFore();
protected slots:
  void setStatus(QString text);
  void populateList(QStringList items);
signals:
  void status(QString);
  void itemSelected(int);
private:
  ConnectionManager * _manager;
  Ui::ConnectionWidget *ui;
};

#endif // CONNECTIONWIDGET_H
