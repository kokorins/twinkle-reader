#ifndef LENTAMANAGER_H
#define LENTAMANAGER_H

#include "lentatypes.h"
#include <QObject>
#include <QString>
#include <QNetworkReply>
#include <QMap>
#include <QVector>
#include <QStringList>
#include <QSslError>

class LocalLenta;

class LentaManager : public QObject {
  Q_OBJECT
public:
  enum EReplyType {
    SN,
    SU,
    F,
    Ps,
    P,
    Fav,
    EReplyTypeLen
  };
public:
  explicit LentaManager(const QString& token, QObject *parent = 0);
  void setLocal(LocalLenta*local);
  void requestMarkRead(QString id);
  void cancel(EReplyType);
  void cancelAll();
signals:
  void summaryDownloading(qint64, qint64);
  void feedsDownloading(qint64, qint64);
  void postsDownloading(qint64, qint64);
  void connectionError();
public slots:
  void requestFavicon(QString feed_md5);
  void requestFeeds();
  void requestSummary();
  void requestPosts(QString feed_md5);
protected slots:
  void summaryUnreadFinished();
  void summaryNewFinished();
  void error(QNetworkReply::NetworkError);
  void summaryDownloadProgress(qint64,qint64);
  void feedsFinished();
  void feedsDownloadProgress(qint64,qint64);
  void postsFinished();
  void postsDownloadProgress(qint64,qint64);
  void updateRead();
  void faviconFinished(QNetworkReply* rep);
private:
  LocalLenta* _local;
  QString _token;
  QMap<EReplyType, QNetworkReply*> _replies;
private:
  const static QString kLentaUrl;
};

#endif // LENTAMANAGER_H
