#ifndef LOCALLENTA_H
#define LOCALLENTA_H

#include "lentatypes.h"
#include <QObject>
#include <QVector>
#include <QMap>
#include <QString>
#include <QStringList>
#include <QReadWriteLock>

/**
 * \brief Local data to be represented.
 * All lenta managers should interact with local lenta object
 */
class LocalLenta : public QObject {
  Q_OBJECT
private:
  const static int kVersion=5;
public:
  explicit LocalLenta(QObject *parent = 0);
public:
  SummaryProperties summary();
  void setSummary(const SummaryProperties& summary);
  void setFeeds(const QVector<FeedInfo>& feeds);
  void updateFeed(int idx, const FeedInfo& fi);
  FeedInfo feed(int idx);
  int searchFeed(FeedInfo::EFeedField field, const QString& val);
  QVector<FeedInfo> feeds();
  QStringList posts(QString feed_md5);
  PostInfo post(QString id);
  void setPost(QString id, const PostInfo& post);
  bool checkPosts(QString feed_md5);
  bool save(QString path, QString name);
  bool load(QString path);
  void clear();
signals:
  void summaryChanged();
  void feedsChanged();
  void postChanged(QString);
private:
  QReadWriteLock _lock;
  SummaryProperties _summary;
  QVector<FeedInfo> _feeds;
  QMap<QString,PostInfo> _posts;
};

#endif // LOCALLENTA_H
