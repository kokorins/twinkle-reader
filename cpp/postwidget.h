#ifndef POSTWIDGET_H
#define POSTWIDGET_H

#include <QtWidgets/QWidget>
#include <QUrl>
#include "postoptions.h"

namespace Ui {
class PostWidget;
}

class PostWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PostWidget(QWidget *parent = 0);
    ~PostWidget();
public:
    void populatePost(QString text);
    void setTitle(QString title);
    virtual void keyPressEvent(QKeyEvent *);
    virtual void mousePressEvent(QMouseEvent *);
    void setOpts(const PostOptions& po);
public slots:
    void goBack();
    void goFore();
protected slots:
    void openExternal(QUrl);
    void openSource();
signals:
    void fore();
    void back();
    void requestSource();
private:
    PostOptions _po;
private:
    Ui::PostWidget *ui;
};

#endif // POSTWIDGET_H
