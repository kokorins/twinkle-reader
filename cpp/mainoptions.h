#ifndef MAINOPTIONS_H
#define MAINOPTIONS_H
#include "twinkleoptions.h"
#include "widgettypes.h"
#include"postoptions.h"
#include <QString>
#include <QObject>
class QIODevice;

/**
 * \brief Session file description
 * All the properties of an application is stored here.
 */
class MainOptions : public QObject {
  Q_OBJECT
public:
  explicit MainOptions(QObject *parent=0);
  void save(QString path, QString name);
  void load(QString path);
protected:
  void save(QIODevice* io);
  void load(QIODevice* io);
public:
  const static int kVersion = 4;
  EProviderType provider;
  int margin;
  int font_size;
  int button_height;
  ReaderType::EReaderTypes rt;
  TwinkleOptions to;
  PostOptions po;
  int progress_timout_msec;
};

#endif // MAINOPTIONS_H
