#ifndef POSTSWIDGET_H
#define POSTSWIDGET_H

#include <QtWidgets/QWidget>
#include <QStateMachine>

namespace Ui {
class PostsWidget;
}

class PostsListModel;
class PostsManager;
class PostsWidget;
class QSortFilterProxyModel;

namespace posts {
class IdleState : public QState {
  Q_OBJECT
public:
  IdleState(PostsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  PostsWidget* _wid;
};

class EmptyState : public QState {
  Q_OBJECT
public:
  EmptyState(PostsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent* event);
private:
  PostsWidget* _wid;
};

class UpdatingState : public QState {
  Q_OBJECT
public:
  UpdatingState(PostsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  PostsWidget* _wid;
};

struct SortingProp {
  QVector<Qt::SortOrder> orders;
  int act_column;
  void changeActivate(int idx);
};
} //namespace posts

class PostsWidget : public QWidget {
  Q_OBJECT
public:
  explicit PostsWidget(QWidget *parent = 0);
  ~PostsWidget();
public:
  virtual void keyPressEvent(QKeyEvent *);
  virtual void setVisible(bool visible);
  void setManager(PostsManager* manager);
public slots:
  void populateList(PostsListModel* model, QString title);
  void setStatus(QString);
protected:
  void sort();
protected slots:
  void goBack();
  void goFore();
  void slotSortAlph();
  void slotSortDate();
signals:
  void indexSelected(int);
  void emptyList();
  void nonEmptyList();
  void back();
  void status(QString);
private:
  friend class posts::IdleState;
  friend class posts::EmptyState;
  friend class posts::UpdatingState;

  Ui::PostsWidget *ui;
  posts::SortingProp sort_prop;
  QSortFilterProxyModel *sorting_proxy;
  PostsManager* _manager;
  QStateMachine _m;
};

#endif // POSTSWIDGET_H
