#ifndef TWINKLEOPTIONS_H
#define TWINKLEOPTIONS_H
#include <QString>
#include <QFont>
#include <QIODevice>

struct TwinkleOptions
{
  enum ENoActivity {
    Default,
    Backlight,
    ENoActivityLen
  };
  static const int kVersion = 2;
  int interval;
  int font_size;
  QFont font;
  QString splitters;
  ENoActivity no_activity;
public:
  TwinkleOptions();
  void save(QIODevice* io);
  void load(QIODevice* io);
};

QString ToString(TwinkleOptions::ENoActivity type);

#endif // TWINKLEOPTIONS_H
