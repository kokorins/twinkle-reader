#ifndef POSTOPTIONS_H
#define POSTOPTIONS_H
#include <QFont>
class QIODevice;


class PostOptions
{
public:
  PostOptions();
  void save(QIODevice* io);
  void load(QIODevice* io);
  void setStylesheetPath(QString path);
  const QString& stylesheetPath() const;
private:
  QString _ss_path;
};

#endif // POSTOPTIONS_H
