#include "twinklewidget.h"
#include "ui_twinklewidget.h"
#include "common.hpp"
#include <QDebug>
#include <QKeyEvent>
#include <algorithm>
#include <functional>

TwinkleWidget::TwinkleWidget(QWidget *parent) : QWidget(parent), ui(new Ui::TwinkleWidget)
{
  ui->setupUi(this);
  TwinkleUtils::SetParentPageStyle(this, parent);
  _idx = 0;
  timer = new QTimer(this);
  timer->setInterval(_to.interval);
  connect(timer, SIGNAL(timeout()), this, SLOT(next()));
  connect(ui->play_button, SIGNAL(clicked()), this, SLOT(startPause()));
  connect(ui->stop_button, SIGNAL(clicked()), this, SLOT(stop()));
  connect(ui->back_button, SIGNAL(clicked()), this, SLOT(goBack()));
  connect(ui->fore_button, SIGNAL(clicked()), this, SLOT(goFore()));
  connect(ui->go_slider, SIGNAL(sliderMoved(int)), this, SLOT(idxSlider(int)));
  connect(ui->go_spin, SIGNAL(valueChanged(int)), this, SLOT(idxSpin(int)));
  connect(ui->ext_button, SIGNAL(clicked()), this, SLOT(openSource()));
  ui->image_label->setVisible(false);
}

TwinkleWidget::~TwinkleWidget()
{
  stop();
  delete ui;
}

void TwinkleWidget::setVisible(bool visible)
{
  QWidget::setVisible(visible);
  if(visible) {
    if(_idx<words.size())
      lookup(words[_idx]);
    else
      lookup(tr("No text"));
  }
}

void TwinkleWidget::stop() {
  if(timer->isActive()) {
    timer->stop();
    timerStopped();
  }
  _idx = 0;
  if(words.empty())
      return;
  lookup(words[_idx]);
}

void TwinkleWidget::next() {
  if(words.empty())
    return;
  lookup(words[_idx++]);
  if(_idx>=words.size()) {
    timer->stop();
    _idx = 0;
    timerStopped();
  }
}

void TwinkleWidget::lookup(const QString& str)
{
  QFont f = ui->text_label->font();
  if(f!=_to.font)
    f = _to.font;
  while(QFontMetrics(f).width(str)>ui->text_label->width())
    f.setPointSize(f.pointSize()/1.2);
  while(QFontMetrics(f).height()>ui->text_label->height())
    f.setPointSize(f.pointSize()/1.2);
  if(ui->text_label->font()!=f)
    ui->text_label->setFont(f);
  qDebug()<<str<<" "<<ui->text_label->width()<<ui->text_label->font().family();
  ui->text_label->setText(str);
  updateWordCount();
}

void TwinkleWidget::timerStarted()
{
  QIcon icon(":icons/pause");
  ui->play_button->setIcon(icon);
  #if defined(Q_OS_SYMBIAN) || defined (Q_OS_ANDROID)
  if(ss_inhibiter)
    delete ss_inhibiter;
  if(_to.no_activity==TwinkleOptions::Backlight) {
    ss_inhibiter = new QTM_PREPEND_NAMESPACE(QSystemScreenSaver)();
    ss_inhibiter->setScreenSaverInhibit();
  }
  #endif
}

void TwinkleWidget::timerStopped()
{
  QIcon icon(":icons/play");
  ui->play_button->setIcon(icon);
  #if defined(Q_OS_SYMBIAN) || defined (Q_OS_ANDROID)
  delete ss_inhibiter;
  ss_inhibiter = 0;
  #endif
}

void TwinkleWidget::startPause() {
  if(timer->isActive()) {
    timer->stop();
    timerStopped();
  }
  else {
    timer->start(_to.interval);
    timerStarted();
  }
}

void TwinkleWidget::pause() {
  if(timer->isActive()) {
    timer->stop();
    timerStopped();
  }
}

void TwinkleWidget::keyPressEvent(QKeyEvent *event)
{
  TwinkleUtils::ContextKeysHandling(event, ui);
}

void TwinkleWidget::goFore()
{
  pause();
  emit fore();
}

void TwinkleWidget::goBack()
{
  stop();
  emit back();
}

void TwinkleWidget::setTitle(QString title)
{
  ui->title_label->setText(title);
}

void TwinkleWidget::setOpts(const TwinkleOptions &to)
{
  _to = to;
  _to.font.setPointSize(_to.font_size);
  qDebug()<<_to.font.family();
  ui->text_label->setFont(_to.font);
}

void TwinkleWidget::setText(QString text)
{
  words = text.split(QRegExp(_to.splitters));
  words.erase(std::remove_if(words.begin(), words.end(), [](const QString& w){return w.isEmpty();}), words.end());
  if(words.empty())
    lookup(tr("No text"));
  else {
    _idx = 0;
    ui->go_slider->setMaximum(words.size()-1);
    ui->go_spin->setMaximum(words.size()-1);
    ui->max_label->setText(tr("%1").arg(words.size()));
    lookup(words.front());
    updateWordCount();
  }
}

void TwinkleWidget::updateWordCount()
{
  ui->go_slider->setValue(_idx);
  ui->go_spin->setValue(_idx);
}

void TwinkleWidget::idxSlider(int idx)
{
  if(idx>=words.size() || idx<0)
    return;
  _idx = idx;
  lookup(words[_idx]);
  updateWordCount();
}

void TwinkleWidget::idxSpin(int idx)
{
  if(idx>=words.size() || idx<0)
    return;
  _idx = idx;
  lookup(words[_idx]);
  updateWordCount();
}

void TwinkleWidget::openSource()
{
  pause();
  emit requestSource();
}
