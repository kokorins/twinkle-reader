#include "mainoptions.h"
#include <QDir>
#include "common.h"

MainOptions::MainOptions(QObject *parent) : QObject(parent)
{
  provider = ConnectionType::Yandex;
  rt = ReaderType::Twinkle;
  margin = 3;
  progress_timout_msec = 5000;
  button_height = 24;
  font_size = 10;
}

void MainOptions::save(QString path, QString name)
{
  QDir dir(path);
  if(!dir.exists(path))
    dir.mkpath(path);
  QFile f(path + name);
  if(f.open(QFile::WriteOnly|QFile::Truncate))
    save(&f);
}

void MainOptions::load(QString path)
{
  QFile f(path);
  if(f.open(QFile::ReadOnly))
    load(&f);
}

void MainOptions::save(QIODevice *io)
{
  TwinkleUtils::WriteInt(io, kVersion);
  TwinkleUtils::WriteInt(io, provider);
  TwinkleUtils::WriteInt(io, margin);
  TwinkleUtils::WriteInt(io, font_size);
  TwinkleUtils::WriteInt(io, rt);
  TwinkleUtils::WriteInt(io, progress_timout_msec);
  TwinkleUtils::WriteInt(io, button_height);
  to.save(io);
  po.save(io);
}

void MainOptions::load(QIODevice *io)
{
  int ver = TwinkleUtils::ReadInt(io);
  if(ver!=kVersion)
    return;
  provider = (EProviderType) TwinkleUtils::ReadInt(io);
  margin = TwinkleUtils::ReadInt(io);
  font_size = TwinkleUtils::ReadInt(io);
  rt = (EReaderTypes) TwinkleUtils::ReadInt(io);
  progress_timout_msec = TwinkleUtils::ReadInt(io);
  button_height = TwinkleUtils::ReadInt(io);
  to.load(io);
  po.load(io);
}
