#include "noactivityeventfilter.h"
#include <QEvent>
#include <QDebug>

NoActivityEventFilter::NoActivityEventFilter(QObject *parent) :
  QObject(parent)
{
  _timer.start();
}

qint64 NoActivityEventFilter::elapsed()
{
  return _timer.elapsed();
}

bool NoActivityEventFilter::eventFilter(QObject *obj, QEvent *ev)
{
  if(ev->type() == QEvent::KeyPress || ev->type() == QEvent::MouseMove) {
    qDebug()<<"No activity for"<<_timer.elapsed()/1000;
    _timer.restart();
  }

  return QObject::eventFilter(obj, ev);
}
