#ifndef SUMMARYWIDGET_H
#define SUMMARYWIDGET_H

#include <QtWidgets/QWidget>

namespace Ui {
class SummaryWidget;
}
class SummaryManager;
struct SummaryProperties;

class SummaryWidget : public QWidget {
  Q_OBJECT
public:
  explicit SummaryWidget(QWidget *parent = 0);
  ~SummaryWidget();
  void setManager(SummaryManager* manager);
public:
  virtual void keyPressEvent(QKeyEvent *event);
protected slots:
  void setProps(const SummaryProperties& props);
public slots:
    void setStatus(const QString& status);
signals:
  void fore();
  void back();
  void options();
  void status(QString);
private:
  SummaryManager *_manager;
  Ui::SummaryWidget *ui;
};

#endif // SUMMARYWIDGET_H
