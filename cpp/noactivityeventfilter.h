#ifndef NOACTIVITYEVENTFILTER_H
#define NOACTIVITYEVENTFILTER_H

#include <QObject>
#include <QElapsedTimer>


class NoActivityEventFilter : public QObject
{
  Q_OBJECT
public:
  explicit NoActivityEventFilter(QObject *parent = 0);
  qint64 elapsed();
protected:
  bool eventFilter(QObject *obj, QEvent *ev);
private:
  QElapsedTimer _timer;
};

#endif // NOACTIVITYEVENTFILTER_H
