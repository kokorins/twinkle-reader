#ifndef OPTIONSWIDGET_H
#define OPTIONSWIDGET_H

#include <QtWidgets/QWidget>
#include <QStateMachine>
#include "widgettypes.h"

namespace Ui {
class OptionsWidget;
}

class OptionsWidget;

namespace options {
class PostState : public QState {
  Q_OBJECT
public:
  PostState(OptionsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  OptionsWidget* _wid;
};

class TwinkleState : public QState {
  Q_OBJECT
public:
  TwinkleState(OptionsWidget *wid, QState *parent = 0) : QState(parent), _wid(wid){}
  virtual void onEntry(QEvent *event);
  virtual void onExit(QEvent *event);
private:
  OptionsWidget* _wid;
};
} //namespace options

class OptionsWidget : public QWidget
{
  Q_OBJECT
  
public:
  explicit OptionsWidget(QWidget *parent = 0);
  ~OptionsWidget();
  void init(EReaderTypes tp);
  virtual void keyPressEvent(QKeyEvent *);
  void setBackWidget(QWidget* back_widget);
  QWidget* backWidget();
protected slots:
  void changeType(int);
signals:
  void post();
  void twinkle();
signals:
  void fore();
  void back();
  void typeChanged(int);
private:
  friend class options::PostState;
  friend class options::TwinkleState;
  QStateMachine _m;
  QWidget* _back;
private:
  Ui::OptionsWidget *ui;
};

#endif // OPTIONSWIDGET_H
