#ifndef POSTSMANAGER_H
#define POSTSMANAGER_H

#include <QObject>
#include <QVector>
#include <QStringList>
#include <QFont>

class LentaManager;
class LocalLenta;
class PostsListModel;

class PostsManager : public QObject {
  Q_OBJECT
public:
  explicit PostsManager(QObject *parent = 0);
  void setLenta(LentaManager* lenta, LocalLenta*local);
  void populateList();
  void setFont(const QFont& font);
  void setCurrentFeed(int feed_idx);
  int currentFeed()const;
  const QStringList& currentPosts()const;
private:
  void populateList(PostsListModel* model, QString title);
protected slots:
  void postsDownloading(qint64,qint64);
public slots:
  void selectItem(int);
  void goBack();
signals:
  void postsUpdated();
  void itemSelected(int);
  void back();
  void progress(QString);
  void requestPopulate(PostsListModel* model, QString title);
private:
  LocalLenta*_local;
  QFont _font;
  QStringList _posts_ids;
  int _feed_idx;
};

#endif // POSTSMANAGER_H
