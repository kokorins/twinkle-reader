#include "postoptions.h"
#include "common.h"
#include <QFont>
#include <QIODevice>

PostOptions::PostOptions()
{
}

void PostOptions::save(QIODevice *io)
{
  TwinkleUtils::WriteString(io, _ss_path);
}

void PostOptions::load(QIODevice *io)
{
  _ss_path = TwinkleUtils::ReadString(io);
}

void PostOptions::setStylesheetPath(QString path)
{
  _ss_path = path;
}

const QString &PostOptions::stylesheetPath()const
{
  return _ss_path;
}

