#include "feedsmanager.h"
#include "lentamanager.h"
#include "locallenta.h"
#include "lentatypes.h"
#include <QDebug>

FeedsManager::FeedsManager(QObject *parent) : QObject(parent) {}

void FeedsManager::setLenta(LocalLenta *local)
{
  _local = local;
}

void FeedsManager::populateList()
{
  FeedsListModel* model = new FeedsListModel(this);
  model->setFeeds(_local->feeds());
  model->setFont(_font);
  populateList(model);
}

void FeedsManager::populateList(FeedsListModel *model)
{
  emit requestPopulate(model);
}

void FeedsManager::setFont(const QFont &font)
{
  _font = font;
}

void FeedsManager::feedsDownloading(qint64 cur, qint64 tot)
{
  emit progress(tr("Downloading %1/%2").arg(cur).arg(tot));
}

void FeedsManager::postsDownloading(qint64 cur, qint64 tot)
{
  emit progress(tr("Downloading %1/%2").arg(cur).arg(tot));
}

void FeedsManager::selectItem(int val)
{
  _val = val;
  QString feed_md5 = _local->feed(val).md5;
  emit itemSelected(_val);
}

void FeedsManager::goBack()
{
  emit back();
}
