#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "twinkleoptions.h"
#include "postoptions.h"
#include "lentatypes.h"
#include "widgettypes.h"

#include <QTimer>
#include <QVector>
#include <QUrl>
#include <QMutex>
#include <QtWidgets/QMainWindow>

namespace Ui {
    class MainWindow;
}

class QNetworkReply;
class OAuthManager;
class LentaManager;
class LocalLenta;
class MainOptions;
class YaTokenWidget;

class ConnectionQml;
class ConnectionManager;
class ConnectionWidget;

class SummaryWidget;
class SummaryManager;

class FeedsManager;
class FeedsWidget;

class PostsManager;
class PostsWidget;

class PostWidget;
class TwinkleWidget;
class OptionsWidget;
class TwinkleOptionsWidget;
class PostOptionsWidget;
class YaCodeWidget;

class MainWindow : public QMainWindow {
  Q_OBJECT
public:
  enum ScreenOrientation {
      ScreenOrientationLockPortrait,
      ScreenOrientationLockLandscape,
      ScreenOrientationAuto
  };

  enum EScreens {
    Login,
    Yandex,
    Summary,
    Feeds,
    Posts,
    Post,
    Options,
    Twinkle,
    TwinOpt
  };

  explicit MainWindow(QWidget *parent = 0);
  virtual ~MainWindow();
  QWidget* readerWidget(EReaderTypes type);
public:
  virtual void keyPressEvent(QKeyEvent *);
  virtual void closeEvent(QCloseEvent *);
signals:
  void status(QString);
public slots:
  void updateSummary();
  void showSummary();
  void updatePosts(int idx);
  void showFeeds();
  void showPosts();
  void showPost(int idx);
  void showOptions();
  void changePostType(int type);
  void showPostOptions();
  void showTwinkleOptions();
  void showSpecialOptions();
  void changeConnType(int value);
  void setStatusText(QString);
  void showConnection();
  void returnTwinkleOptions();
  void returnPostOptions();
  void returnSpecialOptions();
  void returnGeneralOptions();
  void connectionChange();
protected slots:
  void handleAuthError();
  void openSource();
  void hideProgress();
private:
  void setupLogin();
  void setupAuth();
  void setupLocalLenta();
  void setupLenta();
  void setupOptions();
  void setupYaCode();
  void setupSummary();
  void setupFeeds();
  void setupPosts();
  void setupPost();
  void setupPostOptions();
  void setupTwinkle();
  void setupTwinkleOptions();
private:
  const static QString _id;
  const static QString _pass;
  const static QString kLentaUrl;
  int _cur_post;
  QStringList words;
  MainOptions *_opts;
  OAuthManager* _auth;
  LentaManager* _lenta;
  QTimer _t;
  LocalLenta* _local;
private:
  ConnectionManager *_conn_manager;
  SummaryManager * _sum_manager;
  FeedsManager * _feeds_manager;
  PostsManager * _posts_manager;
  ConnectionWidget * conn_widget;
  FeedsWidget * feeds_widget;
  SummaryWidget * summary_widget;
  PostsWidget * posts_widget;

  YaCodeWidget * ya_code_widget;
  PostWidget * post_widget;
  TwinkleWidget * twinkle_widget;
  OptionsWidget * options_widget;
  TwinkleOptionsWidget * twinkle_options_widget;
  PostOptionsWidget * post_options_widget;
  QTimer *_progress_timer;
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
