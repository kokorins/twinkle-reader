#include "connectionmanager.h"

ConnectionManager::ConnectionManager(QObject *parent) : QObject(parent) {}

void ConnectionManager::populateList(QStringList items)
{
  emit listPopulated(items);
}

void ConnectionManager::selectItem(int val)
{
  emit itemSelected(val);
}

void ConnectionManager::goBack()
{
  emit back();
}
