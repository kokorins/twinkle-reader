#ifndef TWINKLEOPTIONSWIDGET_H
#define TWINKLEOPTIONSWIDGET_H

#include "mainwindow.h"
#include "twinkleoptions.h"

namespace Ui {
class TwinkleOptionsWidget;
}

class TwinkleOptionsWidget : public QWidget
{
  Q_OBJECT
  
public:
  explicit TwinkleOptionsWidget(QWidget *parent = 0);
  ~TwinkleOptionsWidget();
  void setBackWidget(QWidget* backWidget);
  QWidget* backWidget();
  TwinkleOptions opts()const;
  void setOpts(const TwinkleOptions& opts);
  virtual void keyPressEvent(QKeyEvent *event);
signals:
  void back();
private:
  QWidget* _back;
private:
  Ui::TwinkleOptionsWidget *ui;
};

#endif // TWINKLEOPTIONSWIDGET_H
