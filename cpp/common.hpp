#ifndef COMMON_HPP
#define COMMON_HPP
#include "common.h"
#include <QKeyEvent>
#include <QDebug>

template <typename T>
void TwinkleUtils::ContextKeysHandling(QKeyEvent* event, T* ui)
{
  if(event->key()==Qt::Key_Context1) {
    if(ui->fore_button->isEnabled ()) {
      ui->fore_button->click();
      event->accept();
    }
    else
      event->ignore();
  }
  else if(event->key()==Qt::Key_Context2) {
    if(ui->back_button->isEnabled ()) {
      ui->back_button->click();
      event->accept();
    }
    else
      event->ignore();
  }
  else {
    event->ignore();
  }
}

#endif // COMMON_HPP
