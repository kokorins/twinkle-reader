#ifndef TWINKLEWIDGET_H
#define TWINKLEWIDGET_H

#include "mainwindow.h"

namespace Ui {
class TwinkleWidget;
}

class TwinkleWidget : public QWidget {
  Q_OBJECT
public:
  explicit TwinkleWidget(QWidget *parent = 0);
  ~TwinkleWidget();
  virtual void setVisible(bool visible);
protected:
  void lookup(const QString& str);
public:
  void timerStopped();
  void timerStarted();
  void pause();
  virtual void keyPressEvent(QKeyEvent *event);
  void setTitle(QString title);
  void setOpts(const TwinkleOptions& to);
  void setText(QString text);
  void updateWordCount();
signals:
  void fore();
  void back();
  void requestSource();
protected slots:
  void openSource();
  void idxSlider(int);
  void idxSpin(int);
public slots:
  void goFore();
  void goBack();
  void stop();
  void next();
  void startPause();
private:
  QTimer* timer;
  QStringList words;
  TwinkleOptions _to;
  int _idx;
  QTimer _no_action;
private:
  Ui::TwinkleWidget *ui;
};

#endif // TWINKLEWIDGET_H
