#include "feedpoller.h"
#include "mainmodel.h"
#include "postsmodel.h"
#include <functional>
#include <QtXml>
#include <QtNetwork>
#include <QNetworkReply>

FeedPoller::FeedPoller(QObject *parent) : QObject(parent), _manager(new QNetworkAccessManager())
{
  connect(_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(slotReplyFinished(QNetworkReply*)));
  FeedParser * fp = new FeedParser();
  fp->moveToThread(&_work_thread);
  connect(this, SIGNAL(sigParse(QUrl, QString)), fp, SLOT(slotParse(QUrl, QString)));
  connect(&_work_thread, SIGNAL(finished()), fp, SLOT(deleteLater()));
  connect(fp, SIGNAL(sigFinishedLoadingFeed(QString,QString,QString)), this, SIGNAL(sigFinishedLoadingFeed(QString,QString,QString)));
  connect(fp, SIGNAL(sigFinishedLoadingPost(Post*)), this, SIGNAL(sigFinishedLoadingPost(Post*)));
  _work_thread.start();
}

FeedPoller::~FeedPoller()
{
  _work_thread.quit();
  _work_thread.wait();
}

void FeedPoller::slotError(QNetworkReply::NetworkError)
{
  if(!sender())
    return;
  QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
  qDebug()<<reply->errorString();
  emit sigError(reply->url(), reply->errorString());
}

void FeedPoller::slotSslError(const QList<QSslError>& ssl_errors)
{
  if(!sender())
    return;
  QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
  qDebug()<<reply->errorString();
  QString err_str;
  foreach(const QSslError& err, ssl_errors)
    err_str.append(err.errorString());
  emit sigError(reply->url(), err_str);
}

void FeedPoller::slotProgress(qint64 cur, qint64 max)
{
  if(!sender())
    return;
  QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
  emit sigProgress(reply->url(), cur, max);
}

void FeedPoller::poll(const QList<QUrl>& feed_urls)
{
  std::for_each(std::begin(feed_urls), std::end(feed_urls),
                [this](const QUrl&url){this->poll(url);});
}

void FeedPoller::poll(const QUrl &feed_url)
{
  QNetworkReply * reply = _manager->get(QNetworkRequest(feed_url));
  connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(slotError(QNetworkReply::NetworkError)));
  connect(reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(slotSslError(QList<QSslError>)));
  connect(reply, SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(slotProgress(qint64,qint64)));
}

void FeedPoller::slotReplyFinished(QNetworkReply *reply)
{
  QString content = QString::fromUtf8(reply->readAll());
  emit sigParse(reply->url(), content);
}

QDateTime FeedParser::PubDateToDateTime(const QString& pub_date_str)
{
  QString pub_date_parse;
  // If the Day is not present in pubDate set a fake Day on top of it to make parsing working.
  if (!pub_date_str.contains(","))
    pub_date_parse = QString("Sun, %1").arg(pub_date_str);
  else
    pub_date_parse = pub_date_str;
  // Parsing the data, take Sun, 12 Oct 2011 15:21:12 GMT
  // pieces[0] is Sun  --- pieces[1] is 12  --- pieces[2] is Oct
  // pieces[3] is 2011 --- pieces[4] is 15:21:12 --- pieces[6] is GMT
  QStringList pieces = pub_date_parse.split(" ");
  QDateTime date;
  int month, year, day;
  // Set the day
  day = pieces.at(1).toInt();
  static QVector<QString> months;
  months<<"Jan"<<"Feb"<<"Mar"<<"Apr"<<"May"<<"Jun"<<"Jul"<<"Aug"<<"Sep"<<"Oct"<<"Nov"<<"Dec";
  if(months.contains(pieces.at(2)))
    month = months.indexOf(pieces.at(2)) + 1;
  // Set the year
  year = pieces.at(3).toInt();
  date.setDate(QDate (year, month, day));
  // Set the hour
  date.setTime(QTime::fromString(pieces[4], "hh:mm:ss"));
  return date;
}

QString FeedParser::OptionalField(const QDomElement &elem, const QString &name)
{
  QDomNodeList nodes = elem.elementsByTagName(name);
  if(nodes.isEmpty()) {
    qDebug()<<"Not found:"<<name;
  }
  else {
    QDomNode l = nodes.item(0);
    if(l.isElement()) {
      return l.toElement().text();
    }
    else {
      qDebug()<<"Found node is not an elem";
    }
  }
  return QString();
}

bool FeedParser::slotParse(const QUrl &feed_url, const QString &content)
{
  // Now update the database with the new data obtained.
  QDomDocument doc;
  if(!doc.setContent(content)) {
    qDebug()<<"Wrong Content"<<content;
    return false;
  }
  QDomElement doc_el = doc.documentElement();
  QString chan_title = doc_el.firstChildElement("channel").firstChildElement("title").text(); // Obligatory
  QString chan_descr = doc_el.firstChildElement("channel").firstChildElement("description").text(); // Obligatory
  emit sigFinishedLoadingFeed(chan_title, feed_url.toString(), chan_descr);
  QDomNodeList items = doc_el.elementsByTagName("item");
  for (int i = 0; i < items.length(); i++) {
    // Get the i-th news
    QDomNode item = items.item(i);
    if(!item.isElement()) {
      qDebug()<<"Item is not an elem";
      continue;
    }
    QDomElement element = item.toElement();
    QString link = OptionalField(element, "link");
    QString title = OptionalField(element, "title");
    QString content = OptionalField(element, "description");
    if (element.elementsByTagName("content:encoded").length() > 0)
      content = element.elementsByTagName("content:encoded").item(0).firstChild().nodeValue();

    // Check if this is RSS2 or not
    uint pub_date;
    QString guid;
    QString pubDateTimeContent;

    // Check if we have Guid, in RSS 1.0 was not present.
    if (element.elementsByTagName("guid").length() != 0)
      guid = element.elementsByTagName("guid").item(0).firstChild().nodeValue();
    else
      guid = link;

    // Check if we have pubDate
    if (element.elementsByTagName("pubDate").length() != 0) {
      pubDateTimeContent = element.elementsByTagName("pubDate").item(0).firstChild().nodeValue();
      pub_date = PubDateToDateTime(pubDateTimeContent).toTime_t();
    }
    else
      pub_date = QDateTime::currentDateTimeUtc().toTime_t();

    Post * post = new Post();
    post->setPubDate(pub_date);
    post->setRead(false);
    post->setTitle(title);
    post->setUrl(QUrl(link));
    post->setContent(content);
    post->setFeed(feed_url);
    emit sigFinishedLoadingPost(post);

//      // Notify of the newly inserted record.
//      if (floodedBubbleQueue) {
//        // Check if enought time has passed.
//        QDateTime now = QDateTime::currentDateTimeUtc();
//        if (now >= lastShownBubble.addSecs(10)) {
//          lastShownBubble = now;
//          sigNewElementsNotification(record.value("title").toString(), record.value("description").toString());
//          floodedBubbleQueue = false;
//        }
//      }
//      else {
//        QDateTime now = QDateTime::currentDateTimeUtc();
//        if (now <= lastShownBubble.addSecs(10)) {
//          floodedBubbleQueue = true;
//          lastShownBubble = now;
//          sigNewElementsNotification(tr("New items to read"), tr("New elements have been loaded in Larss."));
//        }
//        else {
//          lastShownBubble = QDateTime::currentDateTimeUtc();
//          sigNewElementsNotification(record.value("title").toString(), record.value("description").toString());
//        }
//      }
  }
  return true;
}
