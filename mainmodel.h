#ifndef MAINMODEL_H
#define MAINMODEL_H

#include <QObject>
#include <QMap>
#include <QUrl>

class QSystemTrayIcon;
class QQuickCloseEvent;
class QTimer;

class FeedsModel;
class PostsModel;
class Post;
class Feed;
class Twinkle;
struct TwinkleOptions;

struct ModelState {
  enum EPostView {
    Web,
    Twinkle
  };
public:
  ModelState();
public:
  QUrl cur_feed;
  QUrl cur_post;
  QString word;
  EPostView post_view;
  int interval;
};

class MainModel : public QObject {
  Q_OBJECT
  Q_PROPERTY(FeedsModel* feeds_model READ feedsModel CONSTANT)
  Q_PROPERTY(PostsModel* posts_model READ currentFeed CONSTANT)
  Q_PROPERTY(Post* post READ currentPost CONSTANT)
  Q_PROPERTY(QString word READ word WRITE setWord NOTIFY sigWordChanged)
  Q_PROPERTY(QString status READ status WRITE setStatus NOTIFY sigStatusChanged)
  Q_PROPERTY(QUrl cur_feed_url READ currentFeedUrl NOTIFY sigCurFeedUrlChanged)
  Q_PROPERTY(int twinkle_rate READ twinkleRate WRITE setTwinkleRate NOTIFY sigTwinkleOptionsChanged)
  Q_PROPERTY(int post_view READ postView WRITE setPostView NOTIFY sigPostViewChanged)
  Q_PROPERTY(int update_interval READ interval WRITE setInterval NOTIFY sigIntervalChanged)
public:
  explicit MainModel(QObject *parent = 0);
  ~MainModel();
  const QMap<QUrl, Feed*>& feeds()const;
  void setFeeds(const QMap<QUrl, Feed*>& feeds);
  const QMap<QUrl, Post*>& posts()const;
  void setPosts(const QMap<QUrl, Post*>& posts);
  FeedsModel* feedsModel();
  PostsModel* currentFeed();
  Post* currentPost()const;
  QUrl currentFeedUrl()const;
  QString word()const;
  void setWord(const QString& word);
  int twinkleRate()const;
  void setTwinkleRate(int rate);
  void setTwinkleOptions(const TwinkleOptions& to);
  const Twinkle& twinkle()const;
  const QString& status()const;
  void setStatus(const QString& status);
  void updateNumUnread(const QUrl& feed_url);
  int postView()const;
  void setPostView(int post_view);
  int interval()const;
  void setInterval(int interval);
public slots:
  Q_INVOKABLE void slotAddPost(Post* post);
  Q_INVOKABLE void slotAddFeed(const QString& title, const QString& url, const QString &desc);
  Q_INVOKABLE void slotFeedSelected(const QUrl&);
  Q_INVOKABLE void slotRemoveFeed(const QUrl&);
  Q_INVOKABLE void slotPostSelected(const QUrl&);
  Q_INVOKABLE void slotUpdateFeeds();
  Q_INVOKABLE void slotUpdateCurrentFeed();
  Q_INVOKABLE void slotStartTwinkle();
  Q_INVOKABLE void slotStopTwinkle();
  Q_INVOKABLE void slotPauseTwinkle();
  Q_INVOKABLE void slotMarkReadFeed(const QUrl&);
  Q_INVOKABLE void slotSaveSession(const QString& path);
  Q_INVOKABLE void slotLoadSession(const QString& path);
  Q_INVOKABLE void slotOpenSource();
  void slotWordChanged(const QString& word);
  void slotError(const QUrl&, const QString& err);
  void slotProgress(QUrl, qint64,qint64);
  void slotEmptyStatus();
  void slotTrayActivated();
signals:
  void sigUpdateViews();
  void sigWordChanged(QString word);
  void sigTwinkleOptionsChanged();
  void sigCurFeedUrlChanged();
  void sigStatusChanged();
  void sigPostViewChanged();
  void sigIntervalChanged();
private:
  ModelState _state;
  QMap<QUrl,Feed*> _feeds;
  QMap<QUrl,Post*> _posts;
  Twinkle* _twinkle;
  QString _status;
  QTimer* _check_timer;
  QSystemTrayIcon* _tray;
};
#endif // MAINMODEL_H
