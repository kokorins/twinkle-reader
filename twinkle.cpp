#include "twinkle.h"
#include <QDebug>
#include <QTimer>
#include <QRegExp>
#include <QJsonObject>
#include <QtXml>

#include <algorithm>
#include <functional>

QJsonObject TwinkleOptions::toJson() const
{
  QJsonObject opt_obj;
  opt_obj.insert("rate", QJsonValue(rate));
  return opt_obj;
}

bool TwinkleOptions::fromJson(const QJsonObject & opt_obj)
{
  try {
    auto rate_ = opt_obj.value("rate");
    if(!rate_.isDouble())
      return false;
    rate = (int)rate_.toDouble();
  } catch(...) {
    return false;
  }
  return true;
}

TwinkleOptions::TwinkleOptions() : rate(250), splitters("[\\s,\\.]") {}

Twinkle::Twinkle(const TwinkleOptions& to, QObject *parent) : QObject(parent), _to(to)
{
  _state.idx = -1;
  timer = new QTimer(this);
  timer->setInterval(_to.rate);
  connect(timer, SIGNAL(timeout()), this, SLOT(next()));
}

void Twinkle::populateContent(const QString &content)
{
  _state = Parse(content);
}

Twinkle::~Twinkle()
{
  stop();
}

void Twinkle::setOptions(const TwinkleOptions &to)
{
  _to = to;
}

const TwinkleOptions &Twinkle::options() const
{
  return _to;
}

void Twinkle::stop() {
  if(timer->isActive())
    timer->stop();
  _state.idx = -1;
  emit sigCurrentWord("");
}

void Twinkle::next() {
  ++_state.idx;
  if(_state.idx>=_state.words.size())
    stop();
  else {
    qDebug()<<_state.words[_state.idx];
    emit sigCurrentWord(_state.words[_state.idx]);
  }
}

TwinkleState Twinkle::Parse(const QString &content)
{
  QDomDocument doc;
  if(!doc.setContent("<rss>"+content+"</rss>")) {
    qDebug()<<"Wrong Content"<<content;
    TwinkleState ts;
    QString s = content;
    s.remove(QRegExp("<[^>]*>"));
    ts.words = s.split(QRegExp(_to.splitters));
    ts.words.erase(std::remove_if(ts.words.begin(), ts.words.end(),
                                  [](const QString& w){return w.isEmpty();}), ts.words.end());
    return ts;
  }
  QDomElement doc_el = doc.documentElement();
  TwinkleState ts;
  ts.idx = -1;
  ts.words = doc_el.text().split(QRegExp(_to.splitters));
  ts.words.erase(std::remove_if(ts.words.begin(), ts.words.end(),
                                [](const QString& w){return w.isEmpty();}), ts.words.end());
  QDomNodeList links = doc_el.elementsByTagName("a");
  for(int i=0; i<links.size(); ++i) {
    QDomNode l = links.at(i);
    if(l.isElement()) {
      QString title = l.toElement().text();
      QString href = l.toElement().attribute("href");
      if(!title.isEmpty())
        ts.links.insert(title, QUrl(href));
    }
  }
  QDomNodeList imgs = doc_el.elementsByTagName("img");
  for(int i=0; i<imgs.size(); ++i) {
    QDomNode img = imgs.at(i);
    if(img.isElement()) {
      QString alt = img.toElement().text();
      QString href = img.toElement().attribute("href");
      if(!alt.isEmpty())
        ts.imgs.insert(alt, QPixmap(href));
    }
  }
  return ts;
}

void Twinkle::start() {
  if(!timer->isActive())
    timer->start(_to.rate);
}

void Twinkle::pause() {
  if(timer->isActive())
    timer->stop();
}
