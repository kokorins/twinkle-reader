#include "modelstorer.h"
#include "mainmodel.h"
#include "feedsmodel.h"
#include "postsmodel.h"
#include "twinkle.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QFile>
#include <QDebug>
#include <QDir>

void ModelStorer::ToFile(QString file, const MainModel &model)
{
  if(file.startsWith("file:///"))
    file = file.remove(0, 8);
  QFile f(file);
  if(!f.open(QFile::WriteOnly))
    qDebug()<<"Cant open file to write "<<file;
  else
    f.write(ToJson(model).toJson());
}

QJsonDocument ModelStorer::ToJson(const MainModel & model)
{
  QJsonDocument doc;
  QJsonObject root;
  QJsonArray feeds;
  root.insert("version",QJsonValue(1));
  foreach(Feed* f, model.feeds().values())
    feeds.append(f->toJson());
  root.insert("feeds", feeds);
  QJsonArray posts;
  foreach(Post* p, model.posts().values())
    posts.append(p->toJson());
  root.insert("posts", posts);
  root.insert("twinkle_options", model.twinkle().options().toJson());
  root.insert("post_view", model.postView());
  root.insert("update_interval", model.interval());
  doc.setObject(root);
  return doc;
}

MainModel *ModelStorer::FromJson(const QJsonDocument &doc, QObject *parent)
{
  MainModel* res = new MainModel(parent);
  FromJson(doc, *res);
  return res;
}

void ModelStorer::FromJson(const QJsonDocument &doc, MainModel& model)
{
  QJsonObject root = doc.object();
  if(1!=(int)root.value("version").toDouble()) {
    qDebug()<<"Version differs:"<<root.value("version").toDouble();
    return;
  }
  QJsonValue feeds_value = root.value("feeds");
  QJsonValue posts_value = root.value("posts");
  if(!feeds_value.isArray()) {
    qDebug()<<"Feeds are not an array";
    return;
  }
  if(!posts_value.isArray()) {
    qDebug()<<"Posts are not an array";
    return;
  }
  QJsonValue to = root.value("twinkle_options");
  if(!to.isObject()) {
    qDebug()<<"Cant reed twinkle options";
    return;
  }

  QJsonArray feeds_array = feeds_value.toArray();
  QJsonArray posts_array = posts_value.toArray();
  QMap<QUrl, Feed*> feeds;
  for(auto it = feeds_array.begin(); it!= feeds_array.end(); ++it) {
    Feed* f = new Feed(&model);
    if(f->fromJson((*it).toObject())) {
      feeds.insert(f->url(), f);
    }
    else {
      qDebug()<<"Cant load feed:"<<(*it).toString();
    }
  }


  QMap<QUrl, Post*> posts;
  for(auto it = posts_array.begin(); it!=posts_array.end(); ++it) {
    Post* p = new Post(&model);
    if(p->fromJson((*it).toObject())) {
      posts.insert(p->url(), p);
    }
    else {
      qDebug()<<"Cant load post:"<<(*it).toString();
    }
  }

  TwinkleOptions to_;
  if(to_.fromJson(to.toObject())) {
    model.setTwinkleOptions(to_);
  }
  else {
    qDebug()<<"Cant load twinkle options:"<<to.toString();
  }
  auto post_view = root.value("post_view");
  if(!post_view.isDouble()) {
    qDebug()<<"Cant read post_view";
  }
  auto interval = root.value("update_interval");
  if(!interval.isDouble()) {
    qDebug()<<"Cant read update_interval";
  }
  model.setPosts(posts);
  model.setPostView((int)post_view.toDouble(0));
  model.setInterval((int)interval.toDouble(0));
  model.setFeeds(feeds);
}

MainModel *ModelStorer::FromFile(const QString &file, QObject *parent)
{
  MainModel * res = new MainModel(parent);
  FromFile(file, *res);
  return res;
}

void ModelStorer::FromFile(QString file, MainModel& model)
{
  if(file.startsWith("file:///"))
    file = file.remove(0, 8);
  qDebug()<<"Load from:"<<file;
  if(file.isEmpty())
    return;
  QFile f(file);
  if(!f.open(QFile::ReadOnly))
    qDebug()<<"Cant open file to read:"<<file;
  else
    FromJson(QJsonDocument::fromJson(f.readAll()), model);
}
